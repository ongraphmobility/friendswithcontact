//
//  CategoryModel.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 05/12/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import Foundation

struct HomeCategory: Codable {
    let unread_count: Int?
    let categories: [Category]?
}

struct Category: Codable {
    let id: Int?
    let name: String?
    let image: String?
    let childCategories: [ChildCategory]?
    
    enum CodingKeys: String, CodingKey {
        case id, name, image
        case childCategories = "child_categories"
    }
}

struct ChildCategory: Codable {
    let id: Int?
    let name: String?
    let count:Int?
    let image: String?
}
