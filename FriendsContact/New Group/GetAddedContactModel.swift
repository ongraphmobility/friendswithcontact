//
//  GetAddedContactModel.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 07/12/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import Foundation


struct GetAddedContact: Codable {
    let data: [ContactAdded]?
    let success: String?
    
}

struct ContactAdded: Codable {
    let id: Int?
    let tag: String?
    let service_number: String?
    let rating: Double?
    let service_number_id: String?
    let friends_added_by: [FriendsAddedBy]?
    let verified:Bool?
    
    enum CodingKeys: String, CodingKey {
        case id
        case tag
        case service_number
        case rating
        case service_number_id
        case friends_added_by
        case verified
    }
}
struct FriendsAddedBy: Codable {
    let name: String?
    let phone_number: String?
}

struct NameTag: Codable {
    let id: Int?
    let name: String?
}


//////

