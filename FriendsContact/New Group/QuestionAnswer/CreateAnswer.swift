//
//  CreateAnswer.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 17/12/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import Foundation
struct CreateAnswer:Codable {
    
    let message: String?
    let success:String?
    let data:Answer?
}
