//
//  PostQuestion.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 14/12/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import Foundation

struct PostQuestion:Codable {
    
    let message: String?
    let success:String?
    let data:AllQuestion?
}

//struct PhoneBook: Codable {
//
//    let message:String?
//    let success:String?
//   // let new_numbersa:[[String:Any]]?
//}

struct PhoneBook: Codable {
    let message: String?
    let new_numbers: [NewNumber]?
    let success: String?
   
}

struct NewNumber: Codable {
    let phone_number: String
    let user_id: Int
    let name: String
    
    
}
