//
//  GetAllAnswer.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 17/12/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import Foundation

struct GetAllAnswer: Codable {
    let data: [Answer]?
    let success: String?
}

struct Answer: Codable {
    let id: Int?
    let answer: String?
    let answeredBy: AnsweredBy?
    
    enum CodingKeys: String, CodingKey {
        case id, answer
        case answeredBy = "answered_by"
    }
}

struct AnsweredBy: Codable {
    let phoneNumber, name: String?
    
    enum CodingKeys: String, CodingKey {
        case phoneNumber = "phone_number"
        case name
    }
}

