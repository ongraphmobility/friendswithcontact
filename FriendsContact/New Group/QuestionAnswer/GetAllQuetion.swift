//
//  GetAllQuetion.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 14/12/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import Foundation
struct GetAllQuestion: Codable {
    let data: [AllQuestion]?
    let success: String?
}

struct AllQuestion: Codable {
    let id: Int?
    let question: String?
    let posted_by: PostedBy?
    
  
}

struct PostedBy: Codable {
    let phone_number: String?
    let name: String?

}
