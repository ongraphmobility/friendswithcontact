//
//  LoginModel.swift
//  HealthSafetyApp
//
//  Created by Sakshi Singh on 05/10/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import Foundation


// Error model for app
struct ErrorModel: Decodable {
    
    let code:Int
    let status:String
    let message:String
    
    private enum CodingKeys: String, CodingKey {
        case code, status, message
    }
}

//Login model for app

struct LoginModel: Codable {
    
    let message: String?
    let success: String?
    let user:User?
    
}
struct User:Codable {
    let phone_number:Int?
    let user_id:Int?
    let region:String?
    let std_code:String?
    let country_code:String?
    let id:Int?
}

