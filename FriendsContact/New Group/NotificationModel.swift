//
//  NotificationModel.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 11/03/19.
//  Copyright © 2019 Sakshi Singh. All rights reserved.
//


import Foundation

struct NotificationModel: Codable {
    let status: String
    let data: [NotificationList]
}

struct NotificationList: Codable {
    let id: Int?
    let recipient: String?
    let notificationRead: Bool?
    let notificationData: NotificationData?
    let createdOn: String?
    
    enum CodingKeys: String, CodingKey {
        case id, recipient
        case notificationRead = "notification_read"
        case notificationData = "notification_data"
        case createdOn = "created_on"
    }
}

struct NotificationData: Codable {
    let clickAction, tag: String
    let metaData: MetaData
    
    enum CodingKeys: String, CodingKey {
        case clickAction = "click_action"
        case tag
        case metaData = "meta_data"
    }
}

struct MetaData: Codable {
    let body, title: String
    let postedBy: String?
    let screenTag: String
    let question: String?
    let questionID: Int?
    let rating: String?
    let userRecordID: Int?
    let verified: Bool?
    let serviceNumber: String?
    let cid, pid: Int?
    let cidName, pidName: String?
    let pidImage: String?
    let tag, addedByPhone: String?
    let cidImage: String?
    let addedBy: String?
    
    enum CodingKeys: String, CodingKey {
        case body, title
        case postedBy = "posted_by"
        case screenTag = "screen_tag"
        case question
        case questionID = "question_id"
        case rating
        case userRecordID = "user_record_id"
        case verified
        case serviceNumber = "service_number"
        case cid, pid
        case cidName = "cid_name"
        case pidName = "pid_name"
        case pidImage = "pid_image"
        case tag
        case addedByPhone = "added_by_phone"
        case cidImage = "cid_image"
        case addedBy = "added_by"
    }
}
