//
//  DetailsPersonViewController.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 09/01/19.
//  Copyright © 2019 Sakshi Singh. All rights reserved.
//

import UIKit
import KVNProgress
import FloatRatingView
import Contacts
import ContactsUI
import DropDown

class DetailsPersonViewController: UIViewController,CNContactViewControllerDelegate,contactDelegate {
    
    
    var personName:String!
    var personID:String!
    var topicName:String!
    var contactDelegate:ContactDelegate!
    var subCategory:String!
    var personNumber:String!
    var shortNameOfPerson:String!
    var liveLabel: String!
    var updatedLabel: String!
    var phoneNumberSelected = 0
    var ratingPerson:String!
    var contactList:String!
    @IBOutlet weak var lbl_Name:UILabel!
    @IBOutlet weak var lbl_shortName:UILabel!
    @IBOutlet weak var tableView_ContactAdd:UITableView!
    @IBOutlet var floatRatingView: FloatRatingView!
    @IBOutlet weak var totalRatingview:UIStackView!
    @IBOutlet weak var lbl_TotalRating:UILabel!
    
    var isaddedByPhoneNumber = Bool()
    
    var contactDetail:ContactAdded!
    
    //MARK:ViewLife Cycle-------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = personName ?? ""
        self.headernavigation()
        
        //mark: condition for edited button show
        if isaddedByPhoneNumber == false{
            self.navigationItem.rightBarButtonItem = nil
        }else{
            self.navigationItem.rightBarButtonItem = self.navigationItem.rightBarButtonItem
        }
        
        lbl_Name.text = personName ?? ""
        totalRatingview.isHidden = false
        self.lbl_TotalRating.text = ratingPerson ?? ""
        updatedLabel = String(format: "%.1f", self.ratingPerson ?? 0.0)
        liveLabel = String(format: "%.1f", self.ratingPerson ?? 0.0)
        floatRatingView.rating = Double(ratingPerson)!
        
        //mark:For abbrevitaion of name
        var stringNeed = ""
        stringNeed = stringNeed + String(personName.first!)
        lbl_shortName.text = stringNeed
        
        self.ratingStar()
        self.tableView_ContactAdd.tableFooterView = UIView()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:For Edited Delegate value--------------------------------------------------
    func valueLable(rating: String, personName: String, personNumber: String, id: Int) {
        
        self.personName = personName
        self.ratingPerson = rating
        self.personNumber  = personNumber
        
    }
    //MARK:IBAction------------------------------------------------------------------
    @IBAction func btn_Back(sender:UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_More(sender:UIButton){
        
        let dropDown = DropDown()
        dropDown.anchorView = self.navigationItem.rightBarButtonItem // UIView or UIBarButtonItem
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = ["Edit          ", "Delete         "]
        dropDown.textColor = UIColor.black
        dropDown.backgroundColor = UIColor.white
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            if index == 0{
                //mark:for editing value-------------------------------------------------
                let editVC = self.storyboard?.instantiateViewController(withIdentifier: "EditDetailViewController") as! EditDetailViewController
                editVC.name = self.personName ?? ""
                editVC.phoneNumber = (self.personNumber ?? "")
                editVC.rating =  self.ratingPerson ?? ""
                editVC.id = self.contactDetail.id ?? 0
                editVC.ContactEditDelegate = self
                self.navigationController?.pushViewController(editVC, animated: true)
                
            }else{
                //mark:for delete value-------------------------------------------------
                UserDefaults.standard.set(self.contactDetail.id ?? 0, forKey: "id")
                UserDefaults.standard.set(self.personID ?? "" , forKey: "service_number") //Person-number
                
                AKAlertController.alert("Alert", message: "Do you really want to delete Contact?", buttons: ["Ok","Cancel"], tapBlock: { (_, _, index) in
                    if index == 1 {return}
                    self.deleteApi()
                    self.navigationController?.popViewController(animated: true)
                    
                })
            }
        }
        
        // Will set a custom width instead of the anchor view width
        dropDown.width = 300
        dropDown.show()
    }
    @IBAction func btn_AddContacts(sender:UIButton){
       
            let newContact = CNMutableContact()
            newContact.phoneNumbers.append(CNLabeledValue(label: personName, value: CNPhoneNumber(stringValue: personNumber)))
            let contactVC = CNContactViewController(forUnknownContact: newContact)
            contactVC.contactStore = CNContactStore()
            contactVC.delegate = self
            contactVC.allowsActions = false
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigationController!.pushViewController(contactVC, animated: true)
       
       
        
    }
    
    private func contactViewController(viewController: CNContactViewController, didCompleteWithContact contact: CNContact?) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func contactViewController(_ viewController: CNContactViewController, shouldPerformDefaultActionFor property: CNContactProperty) -> Bool {
        return true
    }
}
extension DetailsPersonViewController:UITableViewDelegate,UITableViewDataSource{
    
    //MARK: TableView Delegate & DataSource----------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PersonCellTableViewCell", for: indexPath) as! PersonCellTableViewCell
        
        cell.lbl_Number.text = (self.personNumber ?? "")
        
        let customBGColorView = UIView()
        customBGColorView.backgroundColor = UIColor.init(hex:0x6799F2)//hexString: "#6799F2"
        cell.selectedBackgroundView = customBGColorView
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let numbersOnly = personNumber.replacingOccurrences(of: " ", with: "")
        
        if let url = URL(string: "tel://\(numbersOnly)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    
}
extension DetailsPersonViewController: FloatRatingViewDelegate {
    
    // MARK: FloatRatingViewDelegate---------------------------------------------
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        
        // updatedLabel = String(format: "%.1f", self.ratingPerson ?? 0.0)
        liveLabel = String(format: "%.1f", self.ratingPerson ?? 0.0)
        floatRatingView.rating = Double(ratingPerson)!
        
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        
        updatedLabel = String(format: "%.1f", self.ratingPerson ?? 0.0)
        //  liveLabel = String(format: "%.1f", self.ratingPerson ?? 0.0)
        floatRatingView.rating = Double(ratingPerson)!
    }
    
    func ratingStar(){
        floatRatingView.type = .wholeRatings
        // Reset float rating view's background color
        floatRatingView.backgroundColor = UIColor.clear
        
        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        floatRatingView.delegate = self
        floatRatingView.contentMode = UIViewContentMode.scaleAspectFit
        
        liveLabel = String(format: "%.1f", self.ratingPerson ?? 0.0)
        updatedLabel = String(format: "%.1f", self.ratingPerson ?? 0.0)
        
        
    }
    //MARK: Api's Function Calling----------------------------------------------------
    func deleteApi(){
        
        Network.shared.deleteContact{ (result) in
            //KVNProgress.dismiss()
            guard result != nil else {
                
                return
            }
           
        }
    }
    
}
