//
//  LoginViewController.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 28/11/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit
import CountryPicker
import Firebase
import FirebaseAuth
import KVNProgress


class LoginViewController: UIViewController,CountryPickerDelegate,UITextFieldDelegate {
    
    //MARK:IBOulets for Login
    @IBOutlet weak var lbl_countryCode:UILabel!
    @IBOutlet weak var txt_Number:UITextField!
    @IBOutlet weak var picker: CountryPicker!
    @IBOutlet weak var btn_Loginn: UIButton!
    var countryCodee:String?
    var region:String?
    var stdCode:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       lbl_countryCode.text = "+1"
        //txt_Number.text = "9990792272"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        picker.isHidden = true
         self.navigationController?.isNavigationBarHidden = true
        if let phoneNumber = UserDefaults.standard.value(forKey: "phoneNumber") as? Int{
            self.txt_Number.text = String(phoneNumber)
        }
        if let phonecode = UserDefaults.standard.value(forKey: "phoneCode") as? String{
            self.lbl_countryCode.text = phonecode
        }
    }
   
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        picker.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        picker.isHidden = true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
          picker.isHidden = true
    }

    
    
    //mark:CountryCode picker

    @IBAction func btn_countryPicker(sender:UIButton){
        picker.isHidden = false
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
     
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = true
        picker.setCountry(code!)
    }
    @IBAction func btn_Login(sender:UIButton)
    {

        if (txt_Number.text == "" || txt_Number.text == nil){

           AKAlertController.alert("Alert", message: "Please Enter PhoneNumber")
        }else if (lbl_countryCode.text == "" || lbl_countryCode.text == nil){

            AKAlertController.alert("Alert", message: "Please Enter Country Code")
        }else{

            self.btn_Loginn.isEnabled = false
            let FormatphoneNumber =  (lbl_countryCode.text)! + (txt_Number.text)!
            PhoneAuthProvider.provider().verifyPhoneNumber(FormatphoneNumber, uiDelegate: nil) { (verificationID, error) in

            self.btn_Loginn.isEnabled = false
                if let error = error {

                   self.btn_Loginn.isEnabled = true
                   AKAlertController.alert(error.localizedDescription)
                    //self.showMessagePrompt(error.localizedDescription)
                    return
                }else{
                    UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                    let otpVC = self.storyboard?.instantiateViewController(withIdentifier: "OtpViewController") as! OtpViewController
                    otpVC.phoneNumber = FormatphoneNumber
                        UserDefaults.standard.set("AlreadyLogin", forKey: "AlreadyLogin")
//                  //   otpVC.modalPresentationStyle = UIModalPresentationStyle.popover
//                    //otpVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                    self.navigationController?.pushViewController(otpVC, animated: true)
                }//                // Sign in using the verificationID and the code sent to the user
//                // ...
            }
        }


//
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtpViewController") as! OtpViewController
//        vc.phoneNumber =   (lbl_countryCode.text)! + (txt_Number.text)!
//        UserDefaults.standard.set("AlreadyLogin", forKey: "AlreadyLogin")
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
  
    //MARK:Method's And Functions----------------------------------
    // a picker item was selected
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        //pick up anythink
        lbl_countryCode.text = phoneCode
        self.region = countryCode
        UserDefaults.standard.set(self.region, forKey: "Region")
        UserDefaults.standard.set(lbl_countryCode.text, forKey: "phoneCode")
    }
}
