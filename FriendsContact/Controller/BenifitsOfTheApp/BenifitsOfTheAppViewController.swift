//
//  BenifitsOfTheAppViewController.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 08/01/19.
//  Copyright © 2019 Sakshi Singh. All rights reserved.
//

import UIKit

class BenifitsOfTheAppViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView_About:UITableView!
    
    //MARK:ViewLife Cycle-------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.headernavigation()
        tableView_About.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:UITableViewDelegate & DataSource---------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AboutTableViewCell", for: indexPath) as! AboutTableViewCell
        
        cell.lbl_topic.text = "> No need for background checks. A rating system you can rely on based on personal relationships and experiences of people you trust.\n\n> References cannot be manipulated.\n\n> Friends usually will give a friend better service and pricing.\n\n> No need to make a numerous of phone calls to your friends to find out who they use.\n\n> Privacy - No one sees what services you are looking for. Great for medical references.\n\n> Call people that YOU choose to deal with, not who a website tells you to deal with.\n\n"
         return cell
    }
    
    @IBAction func btn_menu(sender:UIButton){
        
        self.toggleLeft()
    }
    
}
