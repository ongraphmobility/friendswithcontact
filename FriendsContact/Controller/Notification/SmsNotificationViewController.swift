//
//  SmsNotificationViewController.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 04/03/19.
//  Copyright © 2019 Sakshi Singh. All rights reserved.
//

import UIKit
import FloatRatingView
import Contacts
import ContactsUI

class SmsNotificationViewController: UIViewController,UITextViewDelegate,CNContactViewControllerDelegate {
   
    
    var addedBy:String!
    var cidName:String!
    var tagName:String!
    var serviceNumber:String!
    var rating:String!
    var cid:Int!
    var pid:Int!
    var categoryName:String!
    var user_recordId:Int!
    var addedByPhone:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        headernavigation()
        
       
    }
   
    @objc func getMarkItVerified(sender:Any){
        
        self.markVerifyApi()
      
    }
    @IBAction func btn_Back(sender:UIButton){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    func textView(_ textView: UITextView, shouldInteractWith url: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if (url.scheme?.contains("mailto"))! && characterRange.location > 55{
           // openMFMail()
        }
        if (url.scheme?.contains("tel"))! && (characterRange.location > 29 && characterRange.location < 39){
            callNumber()
        }
        return false
    }
    
    func callNumber() {
        
        
            let newContact = CNMutableContact()
            newContact.phoneNumbers.append(CNLabeledValue(label: "", value: CNPhoneNumber(stringValue: self.serviceNumber)))
            let contactVC = CNContactViewController(forUnknownContact: newContact)
            contactVC.contactStore = CNContactStore()
            contactVC.delegate = self
            contactVC.allowsActions = false
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigationController!.pushViewController(contactVC, animated: true)
   }
    
    private func contactViewController(viewController: CNContactViewController, didCompleteWithContact contact: CNContact?) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func contactViewController(_ viewController: CNContactViewController, shouldPerformDefaultActionFor property: CNContactProperty) -> Bool {
        return true
    }
}
extension SmsNotificationViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SmsNotificationTableViewCell", for: indexPath) as! SmsNotificationTableViewCell
       
        let attrs1 = [ NSAttributedString.Key.font: UIFont(name: "Avenir Medium", size: 16.0)!, NSAttributedStringKey.foregroundColor : UIColor.darkGray]

        let attrs2 = [ NSAttributedString.Key.font: UIFont(name: "Avenir Medium", size: 16.0)!, NSAttributedStringKey.foregroundColor : UIColor.blue]

        let attributedString1 = NSMutableAttributedString(string:addedBy, attributes:attrs2)

        let attributedString2 = NSMutableAttributedString(string:" added you to ", attributes:attrs1)
        let attributedString3 = NSMutableAttributedString(string:cidName, attributes:attrs2)
        let attributedString4 = NSMutableAttributedString(string:" category.", attributes:attrs1)

        attributedString1.append(attributedString2)
        attributedString1.append(attributedString3)
        attributedString1.append(attributedString4)
        cell.lbl_Main.attributedText = attributedString1


        cell.btn_Mark.addTarget(self, action: #selector(getMarkItVerified(sender:)), for: .touchUpInside)

        cell.lbl_Name.text = tagName
        cell.lbl_Number.text = serviceNumber
        cell.lbl_Rating.text = rating
//
//        let attributedString5 = NSMutableAttributedString(string:"Please become friends by saving ", attributes:attrs1)
//
//        let attributedString6 = NSMutableAttributedString(string:serviceNumber, attributes:attrs2)
//        let attributedString7 = NSMutableAttributedString(string:" in your phone book in order to find yourself in the", attributes:attrs1)
//        let attributedString8 = NSMutableAttributedString(string:cidName, attributes:attrs2)
//        let attributedString9 = NSMutableAttributedString(string:" category.", attributes:attrs1)
//        attributedString5.append(attributedString6)
//        attributedString5.append(attributedString7)
//        attributedString5.append(attributedString8)
//        attributedString5.append(attributedString9)
//        cell.lbl_Last.attributedText = attributedString5
        let attributedString = NSMutableAttributedString(string: "Please become friends by saving" + " " + addedBy + " " + "in your phone book in order to find yourself in the" + cidName + " " + "category.",attributes:attrs1)
        attributedString.addAttribute(NSAttributedStringKey.link, value: "tel://", range: NSRange(location: 32, length: 10))
    
       cell.lbl_Last.attributedText = attributedString
       cell.lbl_Last.textColor = .black
       cell.lbl_Last.isEditable = false
       cell.lbl_Last.dataDetectorTypes = UIDataDetectorTypes.all
       cell.lbl_Last.delegate = self
        
        
       // cell.lbl_Last.text = "Please become friends by saving" + "+91" + serviceNumber + " " + "in your phone book in order to find yourself in the" + cidName + " " + "category."
       //   cell.lbl_Last.dataDetectorTypes = [.phoneNumber] // doesn't work if textfield isEditable is set to true
          //cell.lbl_Last.linkTextAttributes = [NSForegroundColorAttributeName: UIColor.blue] // the default is blue anyway
        return  cell
    }
    
    func markVerifyApi(){
        UserDefaults.standard.set(user_recordId ?? 0, forKey: "UserIDNotification")
        Network.shared.markVerify{ (result) in
            
            guard let user = result else {
                
                return
            }
           // self.notificationLists = user
            AKAlertController.alert("", message: "Sucessfully added", buttons: ["Ok"], tapBlock: { (_, touch, index) in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddContactsViewController") as! AddContactsViewController
                vc.contactFromNotification = "contactFromNotification"
                vc.cid = self.cid
                vc.pid = self.pid
                vc.serviceNumber = self.addedByPhone ?? ""
                vc.searchNumber = self.serviceNumber ?? ""
                vc.topicName = self.categoryName ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
                
            })
        }
    }
}
    

