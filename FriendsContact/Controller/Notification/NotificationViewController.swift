//
//  NotificationViewController.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 11/03/19.
//  Copyright © 2019 Sakshi Singh. All rights reserved.
//

import UIKit
import KVNProgress

class NotificationViewController: UIViewController {
    
    @IBOutlet weak var tableView:UITableView!
    var notificationLists = [NotificationList]()
    
    var filterData = [String]()
    var filterlistQuestionId = [Dictionary<String,String>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        headernavigation()
        self.notificationListApi()
    }
    @IBAction func btn_Back(sender:UIButton){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    func notificationListApi(){
      KVNProgress.show()
        Network.shared.notificationList{ (result) in
            KVNProgress.dismiss()
            guard let user = result else {
                
                return
            }
           self.notificationLists = user
            if self.notificationLists.count == 0{
                self.tableView.isHidden = true
               // self.lbl_NoContactAdded.isHidden = false
               
            }else{
                self.tableView.isHidden = false
              //  self.lbl_NoContactAdded.isHidden = true
               
            }
            // self.filteredNFLTeams = user
            
            self.tableView.reloadData()
        }
    }
   
    
    func notificationListReadApi(){
        
        Network.shared.notificationListRead{ (result) in
            
            guard result != nil else {
                
                return
            }
          
        }
    }
}

extension NotificationViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return notificationLists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as! NotificationTableViewCell
        let rawData = notificationLists[indexPath.row].notificationData?.metaData
        print(rawData)
        if notificationLists[indexPath.row].notificationRead == true{
            cell.view_Main.backgroundColor = UIColor.white
        }else{
              cell.view_Main.backgroundColor = UIColor.lightGray
        }
        if let photo = rawData?.pidImage, let url = URL(string: photo) {
            cell.img_View.af_setImage(withURL: url)
        } else {
            cell.img_View.image = UIImage(named: "grayscale.png")
        }
        
        cell.lbl_Title.text = rawData?.title
         var string_to_color = String()
        let main_string = rawData?.body
        if rawData?.screenTag == "post_questions"{
            string_to_color = rawData?.postedBy ?? ""
        }else{
            string_to_color = rawData?.addedByPhone ?? ""
        }
       
        
        let range = (main_string! as NSString).range(of: string_to_color)
        let attribute = NSMutableAttributedString.init(string: main_string ?? "")
        attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue , range: range)

        cell.lbl_Description.attributedText = attribute
        
        
        
        let notificationDate = notificationLists[indexPath.row].createdOn
      
        
     
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        
        let dateObj = dateFormatter.date(from: notificationDate!)
        
        dateFormatter.dateFormat = "MMM d, h:mm a"
        print("Dateobj: \(dateFormatter.string(from: dateObj!))")
        
       cell.lbl_Time.text = (dateFormatter.string(from: dateObj!))
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rawData = notificationLists[indexPath.row].notificationData?.tag
        let data = notificationLists[indexPath.row].notificationData
        switch rawData {
        case "post_questions":
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AskQuestionViewController") as! AskQuestionViewController
            vc.indexpathScroll = indexPath.row
            vc.notificationScroll = "scroll"
            if notificationLists[indexPath.row].notificationRead == true{
               // UserDefaults.standard.set("", forKey: "NotificationTapId")
                print("notification Read")
                UserDefaults.standard.set(notificationLists[indexPath.row].id, forKey: "NotificationTapId")
                 self.notificationListReadApi()
            }else{
                
                UserDefaults.standard.set(notificationLists[indexPath.row].id, forKey: "NotificationTapId")
                self.notificationListReadApi()
            }

            
            self.navigationController?.pushViewController(vc, animated: true)
        case "number_verify":
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SmsNotificationViewController") as! SmsNotificationViewController
            if notificationLists[indexPath.row].notificationRead == false{
                // UserDefaults.standard.set("", forKey: "NotificationTapId")
                print("notification Read")
                UserDefaults.standard.set(notificationLists[indexPath.row].id, forKey: "NotificationTapId")
                 self.notificationListReadApi()
            }else{
                
                UserDefaults.standard.set(notificationLists[indexPath.row].id, forKey: "NotificationTapId")
                self.notificationListReadApi()
            }
            vc.addedBy = data?.metaData.addedBy ?? ""
            vc.addedByPhone = data?.metaData.addedByPhone ?? ""
            vc.cidName = data?.metaData.cidName ?? ""
            vc.tagName = data?.tag
            vc.rating =  data?.metaData.rating ?? ""
            vc.serviceNumber = data?.metaData.serviceNumber ?? ""
            vc.cid = data?.metaData.cid ?? 0
            vc.pid = data?.metaData.pid ?? 0
            vc.categoryName = data?.metaData.cidName ?? ""
            vc.user_recordId = data?.metaData.userRecordID
         
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
    
    
}
extension String {
    
    func toDate(withFormat format: String = "yyyy-MM-dd HH:mm:ss")-> Date?{
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Tehran")
        dateFormatter.locale = Locale(identifier: "fa-IR")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)
        
        return date
        
    }
}
