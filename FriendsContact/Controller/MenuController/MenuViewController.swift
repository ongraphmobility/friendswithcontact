//
//  MenuViewController.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 29/11/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var lbl_Name:UILabel!
    let menuArray = ["Home","Notification","About","Getting Started","Benefits Of The App","Post a question to friends","Suggest A Category","Report Issues","Contact Us","Logout"]
    
    let img_Menu = [#imageLiteral(resourceName: "m_aboutus_iPhone"),#imageLiteral(resourceName: "notification"),#imageLiteral(resourceName: "m_aboutus_iPhone"),#imageLiteral(resourceName: "m_about_iPhone"),#imageLiteral(resourceName: "m_gettingstarted_iPhone"),#imageLiteral(resourceName: "m_benefit_iPhone"),#imageLiteral(resourceName: "m_askquestion_iPhone"),#imageLiteral(resourceName: "m_category_icon_iPhone"),#imageLiteral(resourceName: "m_bug_iPhone"),#imageLiteral(resourceName: "m_contactus_iPhone"),#imageLiteral(resourceName: "m_logout_iPhone")]
    
    //MARK:ViewController For LifeCycle----------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
      // Do any additional setup after loading the view.
        
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let phoneNumber = UserDefaults.standard.value(forKey: "phoneNumber") as? Int{
            lbl_Name.text = String(phoneNumber)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCellTableViewCell", for: indexPath) as! MenuCellTableViewCell
        cell.lbl_MenuTitle.text = menuArray[indexPath.row]
        cell.img_Menu.image = img_Menu[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var viewController: UIViewController!
        
        switch indexPath.row {
        case 0:
                viewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        case 1:
    
            viewController = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        case 2:
            viewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutViewController") as! AboutViewController
        case 3:
            viewController = self.storyboard?.instantiateViewController(withIdentifier: "GettingStartedViewController") as! GettingStartedViewController
        case 4:
           
            viewController = self.storyboard?.instantiateViewController(withIdentifier: "BenifitsOfTheAppViewController") as! BenifitsOfTheAppViewController
           
        case 5:
            viewController = self.storyboard?.instantiateViewController(withIdentifier: "AskQuestionViewController") as! AskQuestionViewController
        case 6:
             viewController = self.storyboard?.instantiateViewController(withIdentifier: "SuggestACategoryViewController") as! SuggestACategoryViewController
        case 7:
            viewController = self.storyboard?.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
        case 8:
            viewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController

        case 9:
           logoutApi()
           viewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
           UserDefaults.standard.set("", forKey: "AlreadyLogin")
           UserDefaults.standard.set("false", forKey: "isLoginFristTime")
      //case 9:
         //   viewController = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        default:
            return
        }
        
        let nav = UINavigationController(rootViewController: viewController)
        if let AlreadyLogin = UserDefaults.standard.value(forKey: "AlreadyLogin") as? String{
            
            if AlreadyLogin == "AlreadyLogin"{
                nav.isNavigationBarHidden = false
            }else{
                
                 nav.isNavigationBarHidden = true
            }
        }
        
        nav.isNavigationBarHidden = true
        
        self.slideMenuController()?.mainViewController = nav
        
        self.toggleLeft()
    }
    
    func logoutApi(){
        
        Network.shared.logout{ (result) in
            
            guard result != nil else {
                
                return
            }
    }
}
}
