//
//  PersonContactViewController.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 17/12/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit
import KVNProgress
import FloatRatingView
import Contacts
import ContactsUI
import DropDown
import KVNProgress

//mark:delegate to check from where the flow is coming-------
protocol ContactDelegate {
    func valueLable(contact : String)
}

class PersonContactViewController: UIViewController,CNContactViewControllerDelegate {
    
    var personName:String!
    var topicName:String!
    var contactDelegate:ContactDelegate!
    var subCategory:String!
    var personNumber:String!
    var shortNameOfPerson:String!
    var liveLabel: String!
    var updatedLabel: String!
    var phoneNumberSelected = 0
    var ratingPerson:String!
    var contactList:String!
    @IBOutlet weak var lbl_Name:UILabel!
    @IBOutlet weak var lbl_shortName:UILabel!
    @IBOutlet weak var tableView_ContactAdd:UITableView!
    @IBOutlet var floatRatingView: FloatRatingView!
    @IBOutlet weak var totalRatingview:UIStackView!
    @IBOutlet weak var lbl_TotalRating:UILabel!
    
    
    var personNumberArray = [String]()
    
    //MARK:ViewController LifeCycle------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        lbl_Name.text = personName ?? ""
        if contactList == "ContactList"{
            totalRatingview.isHidden = false
            self.lbl_TotalRating.text = ratingPerson ?? ""
            updatedLabel = String(format: "%.1f", self.ratingPerson ?? 0.0)
            liveLabel = String(format: "%.1f", self.ratingPerson ?? 0.0)
            floatRatingView.rating = Double(ratingPerson)!
            
        }else{
            totalRatingview.isHidden = true
        }
        var stringNeed = ""
        stringNeed = stringNeed + String(personName.first!)
        lbl_shortName.text = stringNeed
        
        self.rating()
        self.tableView_ContactAdd.tableFooterView = UIView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = personName ?? ""
        self.headernavigation()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:IBAction-----------------------------------------------------
    @IBAction func btn_Back(sender:UIButton){
        if subCategory == "subCategory"{
            self.contactDelegate.valueLable(contact: "FromAddContact")
            self.navigationController?.popViewController(animated: true)
            
        }else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func btn_More(sender:UIButton){
        let dropDown = DropDown()
        //DropDown.show()
        
        // The view to which the drop down will appear on
        dropDown.anchorView = view // UIView or UIBarButtonItem
        
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = ["Car", "Motorcycle", "Truck"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
        }
        
        // Will set a custom width instead of the anchor view width
        // dropDownL.width = 200
    }
    @IBAction func btn_AddContacts(sender:UIButton){
        if contactList == "ContactList"{
            //   self.addContactAPi()
            let newContact = CNMutableContact()
            newContact.phoneNumbers.append(CNLabeledValue(label: personName, value: CNPhoneNumber(stringValue: personNumber)))
            let contactVC = CNContactViewController(forUnknownContact: newContact)
            contactVC.contactStore = CNContactStore()
            contactVC.delegate = self
            contactVC.allowsActions = false
            //let navigationController = UINavigationController(rootViewController: contactVC)
            // self.present(navigationController, animated: true, completion: nil)
            self.navigationController?.pushViewController(contactVC, animated: true)
            
        }else{
            if phoneNumberSelected == 0 {
                
                AKAlertController.alert("Alert", message: "Please Select Number")
            }else{
                self.addContactAPi()
                
            }
        }
        
    }
    
    private func contactViewController(viewController: CNContactViewController, didCompleteWithContact contact: CNContact?) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func contactViewController(_ viewController: CNContactViewController, shouldPerformDefaultActionFor property: CNContactProperty) -> Bool {
        return true
    }
    func addContactAPi(){
        var phoneNumberString = String()
        var parentid = Int()
        var childid = Int()
        
        if  let parentID = UserDefaults.standard.value(forKey: "parentid") as? Int{
            parentid = parentID
        }
        if  let childID = UserDefaults.standard.value(forKey: "childId") as? Int{
            childid = childID
        }
        if  let phoneNumber = UserDefaults.standard.value(forKey: "phoneNumber") as? Int {
            phoneNumberString = "\(phoneNumber)"
        }
        
        Network.shared.addContact(parent_category: parentid, child_category: childid, user_phone_number: phoneNumberString, phone_number: personNumber ?? "", rating: updatedLabel ?? "0.0", name_tags: personName, id: 0, service_number: personNumber) { (result) in
         
            guard let user = result else {
                
                return
            }
            
            AKAlertController.alert("", message: "Contact is successfully added to category.", buttons: ["Ok"], tapBlock: { (_, touch, index) in
                
                let viewControllers = self.navigationController!.viewControllers as [UIViewController];
                for aViewController:UIViewController in viewControllers {
                    if aViewController.isKind(of: HomeViewController.self) {
                        _ = self.navigationController?.popToViewController(aViewController, animated: true)
                    }
                }
                
            })
        }
        
        
    }
    
}
extension PersonContactViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if subCategory == "subCategory"{
            return personNumberArray.count
        }
         else if contactList == "ContactList"{
            return 1
        }
        else{
            return personNumberArray.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PersonCellTableViewCell", for: indexPath) as! PersonCellTableViewCell
        if subCategory == "subCategory"{
            cell.lbl_Number.text = (self.personNumberArray[indexPath.row])
        }else if contactList == "ContactList"{
            cell.lbl_Number.text = (self.personNumber ?? "")
        }else{
            cell.lbl_Number.text = (self.personNumberArray[indexPath.row])
        }
        
        let customBGColorView = UIView()
        customBGColorView.backgroundColor = UIColor.init(hex:0x6799F2)//hexString: "#6799F2"
        cell.selectedBackgroundView = customBGColorView
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if contactList == "ContactList"{
            let numbersOnly = personNumber.replacingOccurrences(of: " ", with: "")
            
            if let url = URL(string: "tel://\(numbersOnly)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }else if subCategory == "subCategory"{
            self.personNumber =  (self.personNumberArray[indexPath.row])
            self.phoneNumberSelected = 1
        }
        else{
            self.personNumber =  (self.personNumberArray[indexPath.row])
            self.phoneNumberSelected = 1
        }
        
    }
    
}
extension PersonContactViewController: FloatRatingViewDelegate {
    
    // MARK: FloatRatingViewDelegate
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        if contactList == "ContactList"{
            // updatedLabel = String(format: "%.1f", self.ratingPerson ?? 0.0)
            liveLabel = String(format: "%.1f", self.ratingPerson ?? 0.0)
            floatRatingView.rating = Double(ratingPerson)!
        }else{
            liveLabel = String(format: "%.1f", self.floatRatingView.rating)
        }
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        if contactList == "ContactList"{
            updatedLabel = String(format: "%.1f", self.ratingPerson ?? 0.0)
            //  liveLabel = String(format: "%.1f", self.ratingPerson ?? 0.0)
            floatRatingView.rating = Double(ratingPerson)!
        }else{
            updatedLabel = String(format: "%.1f", self.floatRatingView.rating)
        }
        
    }
    
    
    func rating(){
        
        
        floatRatingView.type = .wholeRatings
        // Reset float rating view's background color
        floatRatingView.backgroundColor = UIColor.clear
        
        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        floatRatingView.delegate = self
        floatRatingView.contentMode = UIViewContentMode.scaleAspectFit
        // floatRatingView.type = .halfRatings
        
        // Segmented control init
        //    ratingSegmentedControl.selectedSegmentIndex = 1
        
        // Labels init
        if contactList == "ContactList"{
            liveLabel = String(format: "%.1f", self.ratingPerson ?? 0.0)
            updatedLabel = String(format: "%.1f", self.ratingPerson ?? 0.0)
            
        }else{
            liveLabel = String(format: "%.1f", self.floatRatingView.rating)
            updatedLabel = String(format: "%.1f", self.floatRatingView.rating)
        }
    }
    
    
}
