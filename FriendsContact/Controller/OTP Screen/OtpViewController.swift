//
//  OtpViewController.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 28/11/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit
import SVPinView
import Firebase
import KVNProgress

class OtpViewController: UIViewController {
    
    @IBOutlet var pinView:SVPinView!
    var phoneNumber:String!
    
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        super.viewDidLoad()
        self.title = "SVPinView"
        configurePinView()
        self.dismissKeyboard()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func configurePinView() {
        
        pinView.pinLength = 6
        pinView.secureCharacter = "\u{25CF}"
        pinView.interSpace = 10
        pinView.textColor = UIColor.black
        pinView.borderLineColor = UIColor.lightGray
        // pinView.activeBorderLineColor = UIColor.lightGray
        pinView.borderLineThickness = 0.5
        
        pinView.allowsWhitespaces = false
        pinView.style = .box
        pinView.fieldBackgroundColor = UIColor.white
        pinView.activeFieldBackgroundColor = UIColor.white
        pinView.fieldCornerRadius = 5
        pinView.activeFieldCornerRadius = 5
        
        pinView.becomeFirstResponderAtIndex = 0
        
        pinView.font = UIFont.systemFont(ofSize: 15)
        //pinView.keyboardType = .phonePad
        //  pinView.didFinishCallback = didFinishEnteringPin(pin:)
        pinView.didFinishCallback = { pin in
            print("The pin entered is \(pin)")
        }
    }
    
    @objc override func dismissKeyboard() {
        self.view.endEditing(false)
    }
    
    
    func didFinishEnteringPin(pin:String) {
        showAlert(title: "Success", message: "The Pin entered is \(pin)")
    }
    
    // MARK: Helper Functions
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func setGradientBackground(view:UIView, colorTop:UIColor, colorBottom:UIColor) {
        for layer in view.layer.sublayers! {
            if layer.name == "gradientLayer" {
                layer.removeFromSuperlayer()
            }
        }
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop.cgColor, colorBottom.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = view.bounds
        gradientLayer.name = "gradientLayer"
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    @IBAction func btn_Verify(sender:UIButton)
    {

                if (pinView.getPin() == "" || pinView.getPin() == nil){

                    AKAlertController.alert("Alert", message: "Please enter otp")
                }else{

                    let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")

                    let credential = PhoneAuthProvider.provider().credential(
                        withVerificationID: verificationID!,
                        verificationCode:pinView.getPin())
                    Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
                        if let error = error {
                            // ...

                           AKAlertController.alert(error.localizedDescription)
                           // self.dismiss(animated: true, completion: nil)
                            return
                        }else{
                            //  AKAlertController.alert("SignUP")
                             self.LoginApis()
//                             UserDefaults.standard.set("true", forKey: "isLoginFristTime")
//                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//
//                            self.navigationController?.pushViewController(vc, animated: true)

                        }

                    }


                }
    //   LoginApis()
        

        
        
    }
    
    
    @IBAction func btn_Resend(sender:UIButton)
    {
        
            let FormatphoneNumber =  phoneNumber ?? ""
            PhoneAuthProvider.provider().verifyPhoneNumber(FormatphoneNumber, uiDelegate: nil) { (verificationID, error) in
                
                
                if let error = error {
                    
                   
                    AKAlertController.alert(error.localizedDescription)
                    //self.showMessagePrompt(error.localizedDescription)
                    return
                }else{
                    UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                   
                }//                // Sign in using the verificationID and the code sent to the user
                //                // ...
            }
      
    }
    
    func LoginApis(){
      //  KVNProgress.show()
        let regionCode = UserDefaults.standard.value(forKey: "Region") as? String
        Network.shared.loginUser(phone_number: phoneNumber ?? "",region: regionCode ?? "IN") { (result) in
        //KVNProgress.dismiss()
            guard let userData = result else {
                
                return
            }
         
            UserDefaults.standard.set(userData.user?.phone_number, forKey: "phoneNumber")
            
            var dataValue = [AnyHashable:Any]()
           
            if let dataComingFromBranch = UserDefaults.standard.value(forKey: "AllUSerDataBranch") as? [AnyHashable:Any]
            {
                dataValue = dataComingFromBranch
                let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "SmsNotificationViewController") as! SmsNotificationViewController
               
                                homeVC.addedBy = dataValue["added_by"] as? String
                                homeVC.cidName = dataValue["cid_name"] as? String
                                homeVC.tagName = dataValue["tag"] as? String
                                homeVC.rating =  dataValue["rating"] as? String
                                homeVC.serviceNumber = dataValue["service_number"] as? String
                                homeVC.cid = dataValue["cid"] as? Int
                                homeVC.pid = dataValue["pid"] as? Int
                                homeVC.categoryName = dataValue["cid_name"] as? String
                                homeVC.user_recordId = dataValue["user_record_id"] as? Int
                
                print(userData.user?.user_id ?? 0)
                   UserDefaults.standard.set(userData.user?.user_id ?? 0, forKey: "user_id")
                UserDefaults.standard.set("true", forKey: "isLoginFristTime")
                 UserDefaults.standard.set("", forKey: "AllUSerDataBranch")
                self.navigationController?.pushViewController(homeVC, animated: true)
                
            }else{
                
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                print(userData.user?.user_id ?? 0)
                vc.UserID = userData.user?.user_id ?? 0
                   UserDefaults.standard.set(userData.user?.user_id ?? 0, forKey: "user_id")
                UserDefaults.standard.set("true", forKey: "isLoginFristTime")
                self.navigationController?.pushViewController(vc, animated: true)

            }
          
            
        }
    }
}


