//
//  GettingStartedViewController.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 08/01/19.
//  Copyright © 2019 Sakshi Singh. All rights reserved.
//

import UIKit

class GettingStartedViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView_About:UITableView!
    
    //MARK:ViewLife Cycle-------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.headernavigation()
        tableView_About.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        
    }
    
    //MARK:UITableViewDelegate & DataSource----------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AboutTableViewCell", for: indexPath) as! AboutTableViewCell
        
        cell.lbl_topic.text = "The first time you use Friends With Contacts (“FWC”), you will need to add the parties/service providers from your contacts that you would like to be accessible through FWC to the app.  This is a one-time process and will not need to be repeated.  In the future, whenever you add a new entry to your contacts, you will receive a prompt asking if you would like to add the new contact to an FWC category.  You can choose whether or not to add the new contact to FWC at that time.\n\n Here are the few simple steps to follow the first time you use FWC:\n\nFIRST, download FWC from the Google Play Store or the App Store.\n\nSECOND, open the App\n\nTHIRD, select a category and, then, a subcategory\n\nFOURTH, select “Add Contacts”\n\nFIFTH, select an entry from your contacts\n\nSIXTH, select a rating for the contact that you are adding to FWC\n\nSEVENTH, select “Add To Contacts”\n\nIt’s that simple!"
        
        
        return cell
    }
    
    @IBAction func btn_menu(sender:UIButton){
        
        self.toggleLeft()
    }
}
