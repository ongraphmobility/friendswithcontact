//
//  AboutViewController.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 28/12/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
   @IBOutlet weak var tableView_About:UITableView!
    
    //MARK:ViewLife Cycle-------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        self.headernavigation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    
        tableView_About.tableFooterView = UIView()
    }
    
    //MARK:UITableViewDelegate & DataSource---------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AboutTableViewCell", for: indexPath) as! AboutTableViewCell
        
             cell.lbl_topic.text = "The idea for Friends With Contacts came about when I received a call from a friend asking if I could recommend someone in the carpet business to install carpeting in his home.  Not only did I know someone I could refer to him but I knew that my friend would be well taken care of.  It then occurred to me that I have many friends in my contact list who probably know someone that I may need to perform a service; however, it might take a long time to locate that particular friend with the right match………Enter Friends With Contacts (“FWC”).\n\nFWC puts two people together in a way that no other social media platform can.  You can only add your personal contacts to FWC and the contacts that you add to FWC are only visible to your personal contacts that are also using FWC and their contacts that are using FWC.  It’s a referral system that provides a level of trust and comfort that you can only get from a personal contact that has first-hand experience with the referenced party.\n\nWhen I thought of my need for a neurosurgeon years back, I realized that I received that reference not from a friend but from a Friend of a Friend.  That reference was successful in case you are wondering and, thus, we decided to create FWC with the ability for users to see not only their friends’ contacts but contacts of friends of their friends (i.e., two levels or degrees). This feature greatly expands the number of contacts available to FWC users and greatly enhances the utility of the app.\n\nIn the coming months keep a look out for updates and amazing new features that will make this site even more valuable to your daily life."
        
       
        return cell
    }
 
    @IBAction func btn_menu(sender:UIButton){
        
        self.toggleLeft()
    }

}
