//
//  AddContactsViewController.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 03/12/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit

class AddContactsViewController: UIViewController,UISearchBarDelegate {
    
    @IBOutlet weak var tableview_AddContacts:UITableView!
    @IBOutlet weak var lbl_NoContactAdded:UILabel!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var topicName:String!
    var filterData = [String]()
    var filteredNFLTeams = [ContactAdded]()
    var array = [ContactAdded]()
    var getContactModel:GetAddedContact?
    var friendByData = [FriendsAddedBy]()
    var friendByDataFilter = [FriendsAddedBy]()
    var isfirstTym = false
    var isaddedByPhoneNumber = false
    let searchController = UISearchController(searchResultsController: nil)
    var cid:Int!
    var pid:Int!
    var serviceNumber:String!
    var searchNumber:String!
    var contactFromNotification:String!
    override func viewDidLoad() {
        super.viewDidLoad()
      
        tableview_AddContacts.tableFooterView = UIView()
        lbl_NoContactAdded.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
        self.headernavigation()
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = UIColor.lightGray
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.black
        
        
        let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = UIColor.darkGray
       
        
        self.hideKeyboardWhenTappedAround()
        contactListApi()
        tableview_AddContacts.reloadData()
        navigationItem.title = topicName ?? ""
        if filteredNFLTeams.count == 0{
            lbl_NoContactAdded.isHidden = true
        }else{
            lbl_NoContactAdded.isHidden = false
        }
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_AddContacts(sender:UIButton){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactListViewController") as! ContactListViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func btn_Back(sender:UIButton){
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func contactListApi(){
        
        if self.contactFromNotification == "contactFromNotification"{
            let serviceNumber = Int(self.serviceNumber)
            UserDefaults.standard.set(pid, forKey: "parentid")
            UserDefaults.standard.set(cid, forKey: "childId")
            //UserDefaults.standard.set(serviceNumber, forKey: "phoneNumber")
        }
        Network.shared.getContact{ (result) in
            // KVNProgress.dismiss()
            guard let user = result else {
                
                return
            }
            self.array = user
            if self.array.count == 0{
                self.tableview_AddContacts.isHidden = true
                self.lbl_NoContactAdded.isHidden = false
                 UserDefaults.standard.set(0, forKey: "FirstContact")
            }else{
                self.tableview_AddContacts.isHidden = false
                self.lbl_NoContactAdded.isHidden = true
                UserDefaults.standard.set(1, forKey: "FirstContact")
            }
           // self.filteredNFLTeams = user
            if self.contactFromNotification == "contactFromNotification"{
            self.searchBar.text = self.searchNumber ?? ""
                self.searchBar(self.searchBar, textDidChange:self.searchNumber ?? "" )
            }
            self.tableview_AddContacts.reloadData()
        }
    }
}
extension AddContactsViewController:UITableViewDelegate,UITableViewDataSource {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredNFLTeams.count == 0{
            return array.count
        }else{
            return filteredNFLTeams.count
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddContactsTableViewCell", for: indexPath) as! AddContactsTableViewCell
        
        if filteredNFLTeams.count == 0{
            
            let stringInput = array[indexPath.row].tag ?? ""
            
            
            let stringInputArr = stringInput.components(separatedBy: " ")
            var stringNeed = ""
            
            for string in stringInputArr {
                let str = (string.count > 0) ? string : " "
                stringNeed = stringNeed + String(str.first!)
            }
            if array[indexPath.row].verified == false{
                
                cell.lbl_Verified.isHidden = true
            }else{
                cell.lbl_Verified.isHidden = false
            }
            cell.lbl_Short.text = stringNeed
            cell.lbl_NAme.text = array[indexPath.row].tag ?? ""
            cell.lbl_Number.text = array[indexPath.row].service_number ?? ""
            cell.lbl_Rating.text = String(array[indexPath.row].rating ?? 0.0)
            self.friendByData = (array[indexPath.row].friends_added_by)!
            let name = friendByData.map{$0.name ?? ""}
            print(name)
           
            let string = name.map { String($0) }
                .joined(separator: ", ")

           
            if array.count != 0 {
                cell.lbl_Show.text = "Added by: " + (string)
            }else{
                cell.lbl_Show.text = "Added by: "
            }

        }else{
            
            let stringInput = filteredNFLTeams[indexPath.row].tag ?? ""
            
            
            let stringInputArr = stringInput.components(separatedBy: " ")
            var stringNeed = ""
            
            for string in stringInputArr {
                let str = (string.count > 0) ? string : " "
                stringNeed = stringNeed + String(str.first!)
            }
            if filteredNFLTeams[indexPath.row].verified == false{
                
                cell.lbl_Verified.isHidden = true
            }else{
                cell.lbl_Verified.isHidden = false
            }
            cell.lbl_Short.text = stringNeed
            cell.lbl_NAme.text = filteredNFLTeams[indexPath.row].tag ?? ""
            cell.lbl_Number.text = filteredNFLTeams[indexPath.row].service_number ?? ""
            cell.lbl_Rating.text = String(filteredNFLTeams[indexPath.row].rating ?? 0.0)
            self.friendByDataFilter = (filteredNFLTeams[indexPath.row].friends_added_by)!
            let name = friendByDataFilter.map{$0.name ?? ""}
            print(name)
            
            
            
            
            let string = name.map { String($0) }
                .joined(separator: ", ")
            
            
            if filteredNFLTeams.count != 0 {
                cell.lbl_Show.text = "Added by: " + (string)
            }else{
                cell.lbl_Show.text = "Added by: "
            }

        }
      
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailsPersonViewController") as! DetailsPersonViewController
   
        vc.contactList = "ContactList"
        if filteredNFLTeams.count == 0{
            vc.ratingPerson = String(array[indexPath.row].rating ?? 0.0)
            vc.personName = self.array[indexPath.row].tag ?? ""
            vc.personNumber = self.array[indexPath.row].service_number ?? ""
            vc.personID = self.array[indexPath.row].service_number_id ?? ""
            vc.contactDetail = self.array[indexPath.row]
            self.friendByData = (array[indexPath.row].friends_added_by)!
            if  let phoneNumberId = UserDefaults.standard.value(forKey: "phoneNumber") as? Int{
                let addedeByPhoneNumber =  friendByData.map{$0.phone_number ?? ""}
                if addedeByPhoneNumber.contains(String(phoneNumberId)){
                    
                    isaddedByPhoneNumber = true
                }else{
                    isaddedByPhoneNumber = false
                }
                
            }
            vc.isaddedByPhoneNumber = isaddedByPhoneNumber
        }else{
            vc.ratingPerson = String(filteredNFLTeams[indexPath.row].rating ?? 0.0)
            vc.personName = self.filteredNFLTeams[indexPath.row].tag ?? ""
            vc.personNumber = self.filteredNFLTeams[indexPath.row].service_number ?? ""
             vc.personID = self.filteredNFLTeams[indexPath.row].service_number_id ?? ""
            vc.contactDetail = self.filteredNFLTeams[indexPath.row]
            self.friendByData = (filteredNFLTeams[indexPath.row].friends_added_by)!
            if  let phoneNumberId = UserDefaults.standard.value(forKey: "phoneNumber") as? Int{
                let addedeByPhoneNumber =  friendByData.map{$0.phone_number ?? ""}
                if addedeByPhoneNumber.contains(String(phoneNumberId)){
                    
                    isaddedByPhoneNumber = true
                }else{
                    isaddedByPhoneNumber = false
                }
                
            }
            vc.isaddedByPhoneNumber = isaddedByPhoneNumber
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
         if self.contactFromNotification == "contactFromNotification"{
            self.filteredNFLTeams = self.array.filter({(item) -> Bool in
                
                ((item.service_number?.range(of:searchBar.text!, options: .caseInsensitive)) != nil)
            })
            
         }else{
            self.filteredNFLTeams = self.array.filter({(item) -> Bool in
                
                ((item.tag?.range(of:searchBar.text!, options: .caseInsensitive)) != nil)
            })
        }
       
        
        
        self.tableview_AddContacts.reloadData()
        
        
    }
    
}
