//
//  MyQuestionViewController.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 14/12/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit

class MyQuestionViewController: UIViewController,UITextViewDelegate{
    
    //MARK:IBOutlets-------------------------------------------------------
    @IBOutlet weak var tableview_MyQuestion:UITableView!
     @IBOutlet weak var no_Questions:UILabel!
    var totalQuestionArray = [AllQuestion]()
    var replymsg = String()
    var question_id = Int()
    var userName = String()
    var questions = String()
    
    //MARK:ViewController LifeCycle---------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
      // Do any additional setup after loading the view.
        self.getAllQuestionListApi()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.headernavigation()
        self.navigationController?.isNavigationBarHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btn_Back(sender:UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func getAllQuestionListApi(){
        
        Network.shared.getMyAllQuestion{ (result) in
            // KVNProgress.dismiss()
            guard let user = result else {
                
                return
            }
            
            self.totalQuestionArray = user.data!
          
            if self.totalQuestionArray.count != 0{
                self.no_Questions.isHidden = true
                self.tableview_MyQuestion.isHidden = false
            }else{
                self.no_Questions.isHidden = false
                self.tableview_MyQuestion.isHidden = true
            }
            self.tableview_MyQuestion.reloadData()
        }
    }
    func createAnswerApi(){
        //  KVNProgress.show()
        
        let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
        Network.shared.createAnswer(question_id: self.question_id, answer: self.replymsg, answered_by_id: userID ?? 0){ (result) in
            //   KVNProgress.dismiss()
            guard result != nil else {
                
                return
            }
            
             AKAlertController.alert("Thank you for submitting your answer.")
        }
        
    }
    
    @objc func send_btn(_ sender:UIButton){
        
        let index = IndexPath(row: sender.tag, section: 0)
        let cell: MyQuestionTableViewCell = self.tableview_MyQuestion.cellForRow(at: index) as! MyQuestionTableViewCell
//        if   (cell.txt_reply.text == "" ||  cell.txt_reply.text == "Reply"){
//            
//            AKAlertController.alert("Can't submit empty reply", message: "")
//        }else{
//           
//        }
        self.replymsg = cell.txt_reply.text
        
        self.createAnswerApi()
        cell.txt_reply.text = ""
        
    }
    @objc func answer_btn(_ sender:UIButton){
        let index = IndexPath(row: sender.tag, section: 0)
        let cell: MyQuestionTableViewCell = self.tableview_MyQuestion.cellForRow(at: index) as! MyQuestionTableViewCell
        UserDefaults.standard.set(self.question_id, forKey: "Question_id")
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AskAnswerViewController") as! AskAnswerViewController
        vc.userName = cell.lbl_USerName.text
        vc.question = cell.lbl_Comment.text
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension MyQuestionViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return totalQuestionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyQuestionTableViewCell", for: indexPath) as! MyQuestionTableViewCell
        
       
        
        cell.txt_reply.toolbarPlaceholder = "Reply"
        cell.tag = indexPath.row
        cell.txt_reply.tag = indexPath.row
        cell.lbl_Comment.tag = indexPath.row
        cell.btn_Answer.tag = indexPath.row
        cell.lbl_USerName.text = totalQuestionArray[indexPath.row].posted_by?.name
        cell.lbl_Comment.text = totalQuestionArray[indexPath.row].question
        self.userName = (totalQuestionArray[indexPath.row].posted_by?.name)!
        self.questions = totalQuestionArray[indexPath.row].question!
        self.question_id = totalQuestionArray[indexPath.row].id ?? 0
        cell.btn_Send.addTarget(self, action: #selector(send_btn(_:)), for: .touchUpInside)
        cell.btn_Answer.addTarget(self, action: #selector(answer_btn(_:)), for: .touchUpInside)
        
        // cell.lbl_USerName.text = totalQuestionArray[indexPath.row]. ""
        
        return cell
    }
    
    
}
