//
//  AskQuestionViewController.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 30/11/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit


protocol SendData {
    func sendData(dataPasser: Any, data: Any)
}

class AskQuestionViewController: UIViewController,UITextViewDelegate,SendData,UIScrollViewDelegate {
   
    
    //MARK:IBOutlets-------------------------------------------------------
    @IBOutlet weak var tableview_Comment:UITableView!
    @IBOutlet weak var txtView_Comment:UITextView!
    @IBOutlet weak var btn_Post:UIButton!
    var commentMessage:String!
    var editMessageIndexPath = 10000
    var commentArray = [String]()
    var totalQuestionArray = [AllQuestion]()
    var arrAllQuestionPost = [Dictionary<String,String>]()
    var selectedcheck = Set<Int>()
    var replymsg = String()
    var question_id = Int()
    var userName = String()
    var questions = String()
    var indexpathScroll:Int!
    var notificationScroll:String!
   
    //MARK:ViewController LifeCycle---------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        txtView_Comment.toolbarPlaceholder = "write a question here...."
//        txtView_Comment.text = "write a question here...."
//        txtView_Comment.textColor = UIColor.lightGray
        tableview_Comment.tableFooterView = UIView()
      
        getAllQuestionListApi()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
        tableview_Comment.estimatedRowHeight = 224
        tableview_Comment.rowHeight = UITableViewAutomaticDimension
         self.headernavigation()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == txtView_Comment {
            if txtView_Comment.text != nil{
             btn_Post.backgroundColor = UIColor.init(hex:  0x546FBA)
            }else{
              btn_Post.backgroundColor = UIColor.lightGray
            }
       
        }

    }

    func textViewDidEndEditing(_ textView: UITextView) {

         if textView == txtView_Comment {
            if (txtView_Comment.text != ""){
                btn_Post.backgroundColor = UIColor.init(hex:  0x546FBA)
            }else{
                btn_Post.backgroundColor = UIColor.lightGray
            }
        }
    }

    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    //MARK:IBACtion---------------------------------------------------
    @IBAction func btn_MyQuestion(sender:UIButton){
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyQuestionViewController") as! MyQuestionViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btn_PostQuestion(sender:UIButton){
        if (txtView_Comment.text == "" || txtView_Comment.text == "write a question here...."){
            
            AKAlertController.alert("Please write a question", message: "")
        }else{
            
            self.postQuestionApi()
            
        }
    }
   
    @IBAction func Menu_btn(Sender:UIButton){
        
        self.toggleLeft()
    }
   
    @objc func edit_btn(_ sender:UIButton){
        
        self.txtView_Comment.text = self.commentMessage ?? ""
    }

    @objc func deleteAction(_ sender: UIButton) {
        AKAlertController.alert("Alert", message: "Are you sure you want to delete this comment", buttons: ["Yes","No"], tapBlock: { (_, _, index) in
            if index == 1 {return}
            self.commentArray.remove(at:sender.tag)
            self.tableview_Comment.reloadData()}
        )}
}
extension AskQuestionViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return totalQuestionArray.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AskQuestionTableViewCell", for: indexPath) as! AskQuestionTableViewCell
        
        cell.txt_reply.delegate = self
        cell.txt_reply.toolbarPlaceholder = "Reply"
        cell.tag = indexPath.row
        cell.txt_reply.tag = indexPath.row
        cell.lbl_USerName.tag = indexPath.row
        cell.lbl_Comment.tag = indexPath.row
        cell.btn_Answer.tag = indexPath.row
        cell.lbl_USerName.text = totalQuestionArray[indexPath.row].posted_by?.name
        cell.lbl_Comment.text = totalQuestionArray[indexPath.row].question
        self.userName = (totalQuestionArray[indexPath.row].posted_by?.name)!
        self.questions = totalQuestionArray[indexPath.row].question!
       // self.question_id = totalQuestionArray[indexPath.row].id ?? 0
        
        let stringInput = totalQuestionArray[indexPath.row].posted_by?.name
        
        let stringInputArr = stringInput?.components(separatedBy: " ")
        var stringNeed = ""
        
        for string in stringInputArr! {
            let str = (string.count > 0) ? string : " "
            stringNeed = stringNeed + String(str.first!)
        }
        cell.lbl_Short.text = stringNeed
        
        cell.btn_Send.addTarget(self, action: #selector(send_btn(_:)), for: .touchUpInside)
        cell.btn_Answer.addTarget(self, action: #selector(answer_btn(_:)), for: .touchUpInside)
        cell.setNeedsUpdateConstraints()
        cell.updateConstraintsIfNeeded()
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        print("scroll first")
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(indexPath.item)
    }
    
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func getAllQuestionListApi(){
        
        Network.shared.getAllQuestion{ (result) in
           
            guard let user = result else {
               return
        }
            
            self.totalQuestionArray = user.data!
        //   self.totalQuestionArray.reverse()
            self.tableview_Comment.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if notificationScroll == "scroll"{
            let indexPath = NSIndexPath(row: indexpathScroll, section: 0)
            tableview_Comment.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
            notificationScroll = ""
        }
//        else{
//            tableview_Comment.scrollToRow(at: <#T##IndexPath#>, at: <#T##UITableViewScrollPosition#>, animated: <#T##Bool#>)
//        }
        

    }
    
    func postQuestionApi(){
     //  KVNProgress.show()
              let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
        Network.shared.postQuestion(posted_by_id: userID!,question: txtView_Comment.text ?? "") { (result) in
             //   KVNProgress.dismiss()
                guard let userData = result else {
                    return
                }
            
            
                self.totalQuestionArray.append(userData)
                self.getAllQuestionListApi()
                self.txtView_Comment.text = ""
               // self.tableview_Comment.reloadData()
          
            }
      
    }
    
    func createAnswerApi(){
        //  KVNProgress.show()
     
        let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
        Network.shared.createAnswer(question_id: self.question_id, answer: self.replymsg, answered_by_id: userID ?? 0){ (result) in
            //   KVNProgress.dismiss()
            guard result != nil else {
                
                return
            }
            
            AKAlertController.alert("Thank you for submitting your answer.")
             
        }
        
    }
    
    @objc func send_btn(_ sender:UIButton){
        
        let index = IndexPath(row: sender.tag, section: 0)
        
        let cell: AskQuestionTableViewCell = self.tableview_Comment.cellForRow(at: index) as! AskQuestionTableViewCell
        question_id = totalQuestionArray[sender.tag].id ?? 0
        if   (cell.txt_reply.text == "" ||   cell.txt_reply.text == "Reply"){
            
            AKAlertController.alert("Can't submit empty reply", message: "")
        }else{
            self.replymsg = cell.txt_reply.text
         
            self.createAnswerApi()
            cell.txt_reply.text = ""
           
        }
       
        
    }
    
    @objc func answer_btn(_ sender:UIButton){
        let index = IndexPath(row: sender.tag, section: 0)
        let cell: AskQuestionTableViewCell = self.tableview_Comment.cellForRow(at: index) as! AskQuestionTableViewCell
      
        
       let vc = self.storyboard?.instantiateViewController(withIdentifier: "AskAnswerViewController") as! AskAnswerViewController
        vc.userName = cell.lbl_USerName.text
        vc.question = cell.lbl_Comment.text
        vc.questionId = totalQuestionArray[sender.tag].id ?? 0
        UserDefaults.standard.set(totalQuestionArray[sender.tag].id ?? 0, forKey: "Question_id")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func sendData(dataPasser: Any, data: Any) {
         if let dataPasser1 = dataPasser as? AskQuestionTableViewCell {
            
        }
    }
    
    
}

