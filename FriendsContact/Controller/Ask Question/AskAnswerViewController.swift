//
//  AskAnswerViewController.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 14/12/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit
import KVNProgress
import IQKeyboardManagerSwift

class AskAnswerViewController: UIViewController,UITextViewDelegate {
    
    //MARK:IBOutlets-------------------------------------------------------
    @IBOutlet weak var lbl_userName:UILabel!
    @IBOutlet weak var tableview_AskAnswerList:UITableView!
    @IBOutlet weak var lbl_question:UILabel!
    @IBOutlet weak var lbl_shortName:UILabel!
    @IBOutlet weak var lbl_Nofound:UILabel!
    @IBOutlet weak var txt_reply:UITextView!
    
    var userName:String!
    var question:String!
    var questionId:Int!
    var anserListArray = [Answer]()
    
    
    //MARK:ViewController LifeCycle---------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
       // Do any additional setup after loading the view.
        self.lbl_userName.text = userName
        self.lbl_question.text = question
       
        getAnswerListApi()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.headernavigation()
   
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        IQKeyboardManager.shared.enable = false
    //  IQKeyboardManager.shared.enableAutoToolbar = true
       txt_reply.toolbarPlaceholder = "Reply"

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

     //MARK:IBActions-------------------------------------------------
    @IBAction func btn_BAck(sender:UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_Send(sender:UIButton){
        
        self.createAnswerApi()
    }
    
    //MARK:Functions,Functions Api & Methods-------------------------------------
    func getAnswerListApi(){
        // KVNProgress.show()
        Network.shared.getAllAnswer{ (result) in
            //KVNProgress.dismiss()
            guard let user = result else {
                
                return
            }
            
            self.anserListArray = user.data!
            if self.anserListArray.count != 0{
                self.lbl_Nofound.isHidden = true
                self.tableview_AskAnswerList.isHidden = false
            }else{
                self.lbl_Nofound.isHidden = false
                self.tableview_AskAnswerList.isHidden = true
            }
            self.tableview_AskAnswerList.reloadData()
        }
    }
    
    
    func createAnswerApi(){
       // KVNProgress.show()
  
        let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
        Network.shared.createAnswerList(question_id: questionId, answer: self.txt_reply.text, answered_by_id: userID ?? 0){ (result) in
          //  KVNProgress.dismiss()
            guard let answerData = result else {
                
                return
            }
           
             self.anserListArray.append(answerData)
           
            self.tableview_AskAnswerList.reloadData()
            self.txt_reply.text = ""
          
       }
        
       
    }
   
}

extension AskAnswerViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return anserListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AskAnswerTableViewCell", for: indexPath) as! AskAnswerTableViewCell
        cell.lbl_userName.text = anserListArray[indexPath.row].answeredBy?.name
        cell.lbl_msg.text = anserListArray[indexPath.row].answer
        return cell
    }
    
    
    
}
