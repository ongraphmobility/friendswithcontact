
import UIKit
import Contacts

class ContactListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate {
    
    //MARK:IBoutlets------------------------------------------------------------
    @IBOutlet var tableView: UITableView!
    var contactListNameArray = [String]()
    var spacecontactListNameArray = [String]()
    var contactListPhoneNumberArray = [String]()
    var shortName:String!
    var homeScreen:String!
    var contactListNamedic = [Dictionary<String,String>]()
    var contactlistFristNumber = [Dictionary<String,String>]()
    
    //var contactlistAll = [FriendsAddedBy]()
    
    @IBOutlet weak var searchBar: UISearchBar!
    let searchController = UISearchController(searchResultsController: nil)
    var filterData = [String]()
    var filterlistFristNumber = [Dictionary<String,String>]()
    
    
    //MARK:Viewcontroller LifeCycles--------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = UIColor.lightGray
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.black
        let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = UIColor.darkGray
        
        AKAlertController.alert("Please select a service number from your phone book.")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.headernavigation()
        contactListNamedic = []
        contactlistFristNumber = []
      
        contactList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-TableView Delegates & Datasources--------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filterlistFristNumber.count == 0{
             return self.contactlistFristNumber.count
        }else{
            return filterlistFristNumber.count
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 	{
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListTableViewCell", for: indexPath) as! ContactListTableViewCell
        
        if filterlistFristNumber.count == 0{
            cell.lbl_Title.text = self.contactlistFristNumber[indexPath.row]["name"]
            cell.lbl_Number.text = self.contactlistFristNumber[indexPath.row]["phone_number"]
            
            //mark:ShortingName---------------------------------------------
            let stringInput = self.contactlistFristNumber[indexPath.row]["name"]
            let stringInputArr = stringInput?.components(separatedBy: " ")
            var stringNeed = ""
            for string in stringInputArr! {
                let str = (string.count > 0) ? string : " "
                stringNeed = stringNeed + String(str.first!)
            }
            cell.lbl_Short.text = stringNeed
        }else{
           cell.lbl_Title.text = self.filterlistFristNumber[indexPath.row]["name"]
            cell.lbl_Number.text = self.filterlistFristNumber[indexPath.row]["phone_number"]
            
            //mark:ShortingName---------------------------------------------
            let stringInput = self.filterlistFristNumber[indexPath.row]["name"]
            let stringInputArr = stringInput?.components(separatedBy: " ")
            var stringNeed = ""
            for string in stringInputArr! {
                let str = (string.count > 0) ? string : " "
                stringNeed = stringNeed + String(str.first!)
            }
            cell.lbl_Short.text = stringNeed
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Row \(indexPath.row) selected")
        if homeScreen == "homeScreen"{
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier:"HomeSubCategoryViewController") as! HomeSubCategoryViewController
            vc.personName = self.contactListNameArray[indexPath.row]
            vc.personNumber = self.contactListPhoneNumberArray[indexPath.row]
            vc.newContactSelect = "newContactSelect"
            
            if filterlistFristNumber.count == 0{
                vc.personName = self.contactListNameArray[indexPath.row]
                
                let test: String = contactlistFristNumber[indexPath.row]["name"]!
                let filteredValue = self.contactListNamedic.filter { (model) -> Bool in
                    return (model["name"])! == test
                }
                
                print(filteredValue)
                let personTotalnumber = filteredValue.map{$0["phone_number"]}
                vc.personNumberArray = personTotalnumber as! [String]
                // vc.personNumber = self.contactListPhoneNumberArray[indexPath.row]
            }else{
                vc.personName = filterlistFristNumber[indexPath.row]["name"]!
                let test: String = filterlistFristNumber[indexPath.row]["name"]!
                let filteredValue = self.contactListNamedic.filter { (model) -> Bool in
                    return (model["name"])! == test
                }
                
                print(filteredValue)
                let personTotalnumber = filteredValue.map{$0["phone_number"]}
                vc.personNumberArray = personTotalnumber as! [String]
                
                //  vc.personNumber = filterlistFristNumber[indexPath.row]["phone_number"]!
            }
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier:"PersonContactViewController") as! PersonContactViewController
            
           
            if filterlistFristNumber.count == 0{
                vc.personName = self.contactListNameArray[indexPath.row]
                
                let test: String = contactlistFristNumber[indexPath.row]["name"]!
                let filteredValue = self.contactListNamedic.filter { (model) -> Bool in
                    return (model["name"])! == test
                }
                
                print(filteredValue)
                let personTotalnumber = filteredValue.map{$0["phone_number"]}
                vc.personNumberArray = personTotalnumber as! [String]
               // vc.personNumber = self.contactListPhoneNumberArray[indexPath.row]
            }else{
                vc.personName = filterlistFristNumber[indexPath.row]["name"]!
                let test: String = filterlistFristNumber[indexPath.row]["name"]!
                let filteredValue = self.contactListNamedic.filter { (model) -> Bool in
                    return (model["name"])! == test
                }
                
                print(filteredValue)
                let personTotalnumber = filteredValue.map{$0["phone_number"]}
                vc.personNumberArray = personTotalnumber as! [String]
                
                 //  vc.personNumber = filterlistFristNumber[indexPath.row]["phone_number"]!
            }
          
          
            
            
         
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    //MARK:Function & Methods-----------------------------------------------------
    func contactList(){
        
        let contactStore = CNContactStore()
        var contacts = [CNContact]()
        let keys = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactPhoneNumbersKey,
            CNContactEmailAddressesKey
            ] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        request.sortOrder = CNContactSortOrder.givenName
        
        do {
            try contactStore.enumerateContacts(with: request){
                (contact, stop) in
                // Array containing all unified contacts from everywhere
                
                contacts.append(contact)
                
                self.contactListNameArray.append(contact.givenName + " " + contact.familyName)
                self.spacecontactListNameArray.append(contact.givenName + contact.familyName)
                self.contactListPhoneNumberArray.append(contact.phoneNumbers.first?.value.stringValue ?? "no number")
                
                var filteredDictionary = [String:String]()
                filteredDictionary["name"] = contact.givenName + " " + contact.familyName
                filteredDictionary["phone_number"] = contact.phoneNumbers.first?.value.stringValue ?? "no number"
                self.contactlistFristNumber.append(filteredDictionary)
               // print(self.contactlistFristNumber)
                
                for phoneNumber in contact.phoneNumbers {
                    if let number = phoneNumber.value as? CNPhoneNumber, let label = phoneNumber.label {
                        _ = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel: label)
                       // print("\(contact.givenName) \(contact.familyName) tel:\(localizedLabel) -- \(number.stringValue)")
                        var emptyDictionary = [String:String]()
                        emptyDictionary["name"] = (contact.givenName + " " + contact.familyName)
                        emptyDictionary["phone_number"] = number.stringValue
                        self.contactListNamedic.append(emptyDictionary)
                       // print(self.contactListNamedic)
                        
                        
                    }
                }
                
                
                
                
            }
           // print(contacts)
            
        } catch {
            print("unable to fetch contacts")
        }
    }
    
    //MARK:IBACtion-----------------------------------------
    @IBAction func btn_Back(sender:UIButton){
        
        self.navigationController?.popViewController(animated: true)
    }
    
//    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        
        self.filterlistFristNumber = contactlistFristNumber.filter {
            return $0["name"]?.range(of: searchText, options: .caseInsensitive) != nil
        }
        // Reload the tableview.
        tableView.reloadData()


    }

   
}

