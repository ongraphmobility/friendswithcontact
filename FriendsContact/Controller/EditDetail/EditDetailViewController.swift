//
//  EditDetailViewController.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 09/01/19.
//  Copyright © 2019 Sakshi Singh. All rights reserved.
//

import UIKit
import FloatRatingView
import SkyFloatingLabelTextField

protocol contactDelegate { //mark: passing new value which is edited to edited controller
    func valueLable(rating : String, personName: String, personNumber:String, id: Int)
}

class EditDetailViewController: UIViewController,UITextViewDelegate {
    
    @IBOutlet var floatRatingView: FloatRatingView!
    @IBOutlet var txt_Name: SkyFloatingLabelTextField!
    @IBOutlet var txt_PhoneNumber: SkyFloatingLabelTextField!
    var liveLabel: String!
    var updatedLabel: String!
    var id: Int!
    var ContactEditDelegate : contactDelegate!
    var name:String!
    var phoneNumber:String!//service_Number
    var rating:String!
    
    //MARK:ViewLife Cycle-------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        txt_Name.text = name
        txt_Name.title = "Name"
        txt_Name.selectedLineColor = UIColor.init(hex:0x6799F2)
        txt_Name.selectedTitleColor = UIColor.init(hex:0x6799F2)
        txt_Name.tintColor = UIColor.init(hex:0x6799F2)
        txt_Name.titleFont.withSize(18.0)
        self.view.addSubview(txt_Name)
        
        txt_PhoneNumber.text = phoneNumber //service_Number
        txt_PhoneNumber.title = "Phone Number"
        txt_PhoneNumber.titleFont.withSize(18.0)
        txt_PhoneNumber.tintColor = UIColor.init(hex:0x6799F2)
        txt_PhoneNumber.selectedTitleColor = UIColor.init(hex:0x6799F2)
        txt_PhoneNumber.selectedLineColor = UIColor.init(hex:0x6799F2)
        floatRatingView.rating = Double(rating)!
        self.view.addSubview(txt_PhoneNumber)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        
        self.headernavigation()
        self.ratingfunction()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:IBAction-----------------------------------------------------
    @IBAction func btn_Back(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btn_Save(sender:UIButton){
        self.addContactAPi()
    }
    
}
extension EditDetailViewController: FloatRatingViewDelegate {
    
    // MARK: FloatRatingViewDelegate
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        
        liveLabel = String(format: "%.1f", self.floatRatingView.rating)
        
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        
        updatedLabel = String(format: "%.1f", self.floatRatingView.rating)
    }
    
    //MARK:Rating function called on viewdidLoad--------------------------------------
    func ratingfunction(){
        floatRatingView.type = .wholeRatings
        // Reset float rating view's background color
        floatRatingView.backgroundColor = UIColor.clear
        
        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        floatRatingView.delegate = self
        floatRatingView.contentMode = UIViewContentMode.scaleAspectFit
        liveLabel = String(format: "%.1f", self.floatRatingView.rating)
        updatedLabel = String(format: "%.1f", self.floatRatingView.rating)
        
    }
    
    //MARK: Api's Function Calling---------------------------------------------------
    func addContactAPi(){ //mark: saveing edited value api
        var phoneNumberString = ""
        var serviceNumber = ""
        let parentID = UserDefaults.standard.value(forKey: "parentid") as? Int
        let childID = UserDefaults.standard.value(forKey: "childId") as? Int
        if  let phoneNumber = UserDefaults.standard.value(forKey: "phoneNumber") as? Int {
            phoneNumberString = "\(phoneNumber)"
        }
        //mark:condition for service number or userphonenumber check
        if txt_PhoneNumber.text != phoneNumber{
            serviceNumber = txt_PhoneNumber.text!
        }else{
            serviceNumber = phoneNumber
        }
        
        Network.shared.addContact(parent_category: parentID!, child_category: childID!, user_phone_number: phoneNumberString, phone_number: txt_PhoneNumber.text ?? "", rating: updatedLabel ?? "0.0", name_tags: txt_Name.text ?? "", id: id ?? 0, service_number: serviceNumber ) { (result) in
            
            guard result != nil else {
                
                return
            }
            AKAlertController.alert("", message: "Contact Edited", buttons: ["Ok"], tapBlock: { (_, touch, index) in
                //mark:edited new value and send back to detail screen
                self.ContactEditDelegate.valueLable(rating: self.updatedLabel ?? "0.0", personName: self.txt_Name.text ?? "", personNumber: self.txt_PhoneNumber.text ?? "", id: self.id ?? 0)
                self.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    
}
