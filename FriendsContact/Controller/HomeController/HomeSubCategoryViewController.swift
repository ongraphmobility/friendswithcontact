//
//  HomeSubCategoryViewController.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 29/11/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit
import AlamofireImage
import KVNProgress

class HomeSubCategoryViewController: UIViewController,UISearchBarDelegate,ContactDelegate {
    
    //MARK:IBOulets for HomeSubCategoryViewController-------------------------
    @IBOutlet weak var collectionview_Category:UICollectionView!
    @IBOutlet weak var collectionview_SubCategory:UICollectionView!
    @IBOutlet weak var lbl_NoCategory:UILabel!
    
    var searchBar = UISearchBar()
    var searchBarButtonItem: UIBarButtonItem?
    var logoImageView   : UIImageView!
    var personNumberArray = [String]()
    
    var personName:String!
    var personNumber:String!
    var newContactSelect:String?
    var homeScreen:String!
    
    var IndexPathSelected:Int!
    var categoryid:Int!
    let categoryTitleArray = ["Home & Yard","Health","Travel","Auto","B2B","Financial","Real Estate","Technology","Entertainment","Pets","Restaurants","Wedding","Education","Sports"]
    let titleImageArray = [#imageLiteral(resourceName: "home_yard_iPhone"),#imageLiteral(resourceName: "helath_iPhone"),#imageLiteral(resourceName: "travel_iPhone"),#imageLiteral(resourceName: "auto_iPhone"),#imageLiteral(resourceName: "b2b_iPhone"),#imageLiteral(resourceName: "financial_iPhone"),#imageLiteral(resourceName: "real_state_iPhone"),#imageLiteral(resourceName: "technology_iPhone"),#imageLiteral(resourceName: "entertainment_iPhone"),#imageLiteral(resourceName: "pets_iPhone"),#imageLiteral(resourceName: "resturant_iPhone"),#imageLiteral(resourceName: "wedding_iPhone"),#imageLiteral(resourceName: "education_iPhone"),#imageLiteral(resourceName: "sports_iPhone")]
    let selectedTitleImageArray = [#imageLiteral(resourceName: "home_yard_selected_iPhone"),#imageLiteral(resourceName: "helath_selected_iPhone"),#imageLiteral(resourceName: "travel_selected_iPhone"),#imageLiteral(resourceName: "auto_selected_iPhone"),#imageLiteral(resourceName: "b2b_selected_iPhone"),#imageLiteral(resourceName: "financial_selected_iPhone"),#imageLiteral(resourceName: "real_state_selected_iPhone"),#imageLiteral(resourceName: "technology_selected_iPhone"),#imageLiteral(resourceName: "entertainment_selected_iPhone"),#imageLiteral(resourceName: "pets_selected_iPhone"),#imageLiteral(resourceName: "resturant_selected_iPhone"),#imageLiteral(resourceName: "wedding_selected_iPhone"),#imageLiteral(resourceName: "education_selected_iPhone"),#imageLiteral(resourceName: "sports_selected_iPhone")]
    var selecteCategoryArray = 0
    var categoryListArray = [Category]()
    var cateegorysubListArray = [ChildCategory]()
    var filteredsubListArray = [ChildCategory]()
    
    //MARK:ViewControllerLifeCycle-------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if newContactSelect == "newContactSelect"{
            AKAlertController.alert("Select a Category")
        }
        // Do any additional setup after loading the view.
        //self.selecteCategoryArray = IndexPathSelected
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        self.headernavigation()
        KVNProgress.dismiss()
        self.categoryList()
        collectionview_SubCategory.reloadData()
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.minimal
        searchBarButtonItem = navigationItem.rightBarButtonItem
       
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    //MARK:IBACtion------------------
    @IBAction func Menu_btn(Sender:UIButton)
    {
        self.toggleLeft()
    }
    @IBAction func searchButtonPressed(sender: AnyObject) {
        showSearchBar()
    }
    
    //mark:delegate for newcontacts----------------
    func valueLable(contact: String) {
        self.newContactSelect = contact
    }
    func showSearchBar() {
        searchBar.alpha = 0
        navigationItem.titleView = searchBar
        
        UIView.animate(withDuration: 0.5, animations: {
            self.searchBar.alpha = 1
        }, completion: { finished in
            self.searchBar.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        navigationItem.setLeftBarButton(searchBarButtonItem, animated: true)
        
        UIView.animate(withDuration: 0.3, animations: {
            // self.navigationItem.titleView = self.logoImageView
            // self.logoImageView.alpha = 1
        }, completion: { finished in
            
        })
    }
    
    
    //MARK: UISearchBarDelegate
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        hideSearchBar()
    }
    
    
}
extension HomeSubCategoryViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionview_Category{
            return categoryListArray.count
        }else if collectionView == collectionview_SubCategory{
            if filteredsubListArray.count == 0{
                return cateegorysubListArray.count
            }else{
                return filteredsubListArray.count
            }
            
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //MARK:Collection Category------------------------------------------
        if collectionView == collectionview_Category{
            let homeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeSelectCategoryCollectionViewCell", for: indexPath) as! HomeSelectCategoryCollectionViewCell
            homeCell.lbl_Title.text = categoryListArray[indexPath.item].name
            
            if let photo = categoryListArray[indexPath.item].image, let url = URL(string: photo) {
                homeCell.img_Logo.af_setImage(withURL: url)
            } else {
                homeCell.img_Logo.image = UIImage(named: "grayscale.png")
            }
            
            homeCell.img_Logo.layer.cornerRadius = 30
            homeCell.img_Logo.layer.masksToBounds = true
            
            if categoryid == self.categoryListArray[indexPath.row].id{
                homeCell.view_Tick.isHidden = false
                let parentid = self.categoryListArray[selecteCategoryArray].id ?? 0
                UserDefaults.standard.set(parentid, forKey: "parentid")
                
            }else{
                homeCell.view_Tick.isHidden = true
            }
            return homeCell
            
        }
        //MARK:Collection Category------------------------------------------
        else{
            let homeSubCell = collectionview_SubCategory.dequeueReusableCell(withReuseIdentifier: "HomeSubCategoryCollectionViewCell", for: indexPath) as! HomeSubCategoryCollectionViewCell
            
            if filteredsubListArray.count == 0{
                homeSubCell.lbl_Title.text = " " + cateegorysubListArray[indexPath.item].name!
                if cateegorysubListArray[indexPath.item].count == 0{
                    let count = cateegorysubListArray[indexPath.item].count ?? 0
                    homeSubCell.lbl_PointFirst.text =  "\(count)"
                    homeSubCell.lbl_First.isHidden = false
                
                }else{
                    homeSubCell.lbl_First.isHidden = true
                    let count = cateegorysubListArray[indexPath.item].count ?? 0
                    homeSubCell.lbl_PointFirst.text =  "\(count)"
                }
                if let photo = cateegorysubListArray[indexPath.item].image, let url = URL(string: photo) {
                    homeSubCell.img_SubCategory.af_setImage(withURL: url)
                } else {
                    homeSubCell.img_SubCategory.image = UIImage(named: "grayscale.png")
                }
                homeSubCell.img_SubCategory.layer.cornerRadius = 40
                homeSubCell.img_SubCategory.layer.masksToBounds = true
                
                
            }
            
                
            else{
                homeSubCell.lbl_Title.text = " " + filteredsubListArray[indexPath.item].name!
                if filteredsubListArray[indexPath.item].count == 0{
                    let count = filteredsubListArray[indexPath.item].count ?? 0
                    homeSubCell.lbl_PointFirst.text =  "\(count)"
                    homeSubCell.lbl_First.isHidden = false
                }else{
                    homeSubCell.lbl_First.isHidden = true
                    let count = filteredsubListArray[indexPath.item].count ?? 0
                    homeSubCell.lbl_PointFirst.text =   "\(count)"
                }
                
                if let photo = filteredsubListArray[indexPath.item].image, let url = URL(string: photo) {
                    homeSubCell.img_SubCategory.af_setImage(withURL: url)
                } else {
                    homeSubCell.img_SubCategory.image = UIImage(named: "grayscale.png")
                }
                homeSubCell.img_SubCategory.layer.cornerRadius = 40
                homeSubCell.img_SubCategory.layer.masksToBounds = true
                
            }
            
            return homeSubCell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionview_Category{
            self.selecteCategoryArray = indexPath.item
            self.categoryid = self.categoryListArray[indexPath.row].id ?? 0
            let parentid = self.categoryListArray[indexPath.row].id ?? 0
            UserDefaults.standard.set(parentid, forKey: "parentid")
            
            categoryList()
            
            print("collectionView Reload")
        }else{
            
            if newContactSelect == "newContactSelect"{
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PersonContactViewController") as! PersonContactViewController
                if filteredsubListArray.count == 0{
                    vc.topicName = self.cateegorysubListArray[indexPath.item].name ?? ""
                    let childId = self.cateegorysubListArray[indexPath.item].id ?? 0
                    vc.personName = personName
                    vc.personNumber = personNumber
                    vc.personNumberArray = personNumberArray
                    vc.contactDelegate = self
                    vc.subCategory = "subCategory"
                    UserDefaults.standard.set(childId, forKey: "childId")
                }else{
                    vc.topicName = self.filteredsubListArray[indexPath.item].name ?? ""
                    let childId = self.filteredsubListArray[indexPath.item].id ?? 0
                    vc.personName = personName
                    vc.personNumber = personNumber
                    vc.personNumberArray = personNumberArray
                    vc.contactDelegate = self
                    vc.subCategory = "subCategory"
                    UserDefaults.standard.set(childId, forKey: "childId")
                }
                
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddContactsViewController") as! AddContactsViewController
                if filteredsubListArray.count == 0{
                    vc.topicName = self.cateegorysubListArray[indexPath.item].name ?? ""
                    let childId = self.cateegorysubListArray[indexPath.item].id ?? 0
                    UserDefaults.standard.set(childId, forKey: "childId")
                }else{
                    vc.topicName = self.filteredsubListArray[indexPath.item].name ?? ""
                    let childId = self.filteredsubListArray[indexPath.item].id ?? 0
                    UserDefaults.standard.set(childId, forKey: "childId")
                }
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == collectionview_SubCategory{
            
            collectionview_SubCategory.cellForItem(at: indexPath)?.backgroundColor = UIColor.white
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionview_Category{
            return CGSize(width: collectionView.bounds.size.width/4 - 14 , height: 100)
        }else{
            return CGSize(width: collectionView.bounds.size.width/2 - 20 , height: 180)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets{
        return  UIEdgeInsetsMake( 5,  14, 5,  14)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func categoryList(){
        if newContactSelect == "newContactSelect"{
            KVNProgress.dismiss()
        }else{
            KVNProgress.show()
        }
       
        Network.shared.categoryUser{ (result) in
            KVNProgress.dismiss()
            guard let user = result else {
                
                return
            }
            self.categoryListArray = user.categories!
            
            for i in 0..<self.categoryListArray.count {
                if self.categoryid == self.categoryListArray[i].id{
                    
                    self.selecteCategoryArray = i
                    self.cateegorysubListArray = self.categoryListArray[self.selecteCategoryArray].childCategories!
                    self.filteredsubListArray = self.categoryListArray[self.selecteCategoryArray].childCategories!
                    
                    if self.cateegorysubListArray.count == 0{
                        self.lbl_NoCategory.isHidden = false
                        self.collectionview_SubCategory.isHidden = true
                        
                    }else{
                        self.lbl_NoCategory.isHidden = true
                        self.collectionview_SubCategory.isHidden = false
                    }
                    
                    self.collectionview_SubCategory.reloadData()
                }
            }
            
            self.collectionview_SubCategory.reloadData()
            self.collectionview_Category.reloadData()
        }
    }
    
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        self.filteredsubListArray = self.cateegorysubListArray.filter({(item) -> Bool in
            
            ((item.name?.range(of:searchBar.text!, options: .caseInsensitive)) != nil)
        })
        
        
        self.collectionview_SubCategory.reloadData()
        
        
    }
}
