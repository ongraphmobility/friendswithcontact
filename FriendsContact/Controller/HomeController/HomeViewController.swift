
//
//  HomeViewController.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 28/11/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI
import KVNProgress

class HomeViewController: UIViewController,UISearchBarDelegate,CNContactPickerDelegate {
    
    //MARK:IBOulets for Login
    @IBOutlet weak var collectionview:UICollectionView!
    @IBOutlet weak var alertView:UIView!
    //mark:variables------------------------
    var searchBar = UISearchBar()
    var searchBarButtonItem: UIBarButtonItem?
    var logoImageView   : UIImageView!
    var contactListNamedic = [Dictionary<String,String>]()
    var contactListPhoneNumberArray = [NewNumber]()
   
    let fcmTokenID = UserDefaults.standard.value(forKey: "fcmtoken") as? String
    let deviceToken = UserDefaults.standard.value(forKey: "deviceToken") as? String
    let titleArray = ["Home & Yard","Health","Travel","Auto","B2B","Financial","Real Estate","Technology","Entertainment","Pets","Restaurants","Wedding","Education","Sports"]
    let titleImageArray = [#imageLiteral(resourceName: "home_yard_iPhone"),#imageLiteral(resourceName: "helath_iPhone"),#imageLiteral(resourceName: "travel_iPhone"),#imageLiteral(resourceName: "auto_iPhone"),#imageLiteral(resourceName: "b2b_iPhone"),#imageLiteral(resourceName: "financial_iPhone"),#imageLiteral(resourceName: "real_state_iPhone"),#imageLiteral(resourceName: "technology_iPhone"),#imageLiteral(resourceName: "entertainment_iPhone"),#imageLiteral(resourceName: "pets_iPhone"),#imageLiteral(resourceName: "resturant_iPhone"),#imageLiteral(resourceName: "wedding_iPhone"),#imageLiteral(resourceName: "education_iPhone"),#imageLiteral(resourceName: "sports_iPhone")]
    var categoryListArray = [Category]()
    var categoryValue = 0
    var filteredCategoryListArray = [Category]()
    let userID = UserDefaults.standard.value(forKey: "user_id") as? Int
    var UserID:Int!
    
    //MARK:ViewController LifeCycle--------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // NotificationCenter.default.addObserver(self, selector: #selector(contactList), name: Notification.Name("NewNumber"), object: nil)
        self.navigationController?.isNavigationBarHidden = false
        alertView.isHidden = true
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    
        
                if let loginFirstTym = UserDefaults.standard.value(forKey: "isLoginFristTime") as? String{
                    if loginFirstTym == "true"{
                        alertView.isHidden = false
                        self.navigationController?.isNavigationBarHidden = true
                    }else{
                        alertView.isHidden = true
                        self.navigationController?.isNavigationBarHidden = false
                    }
        
                }
        self.headernavigation()
        self.hideKeyboardWhenTappedAround()
 

        self.categoryListApi()
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.minimal
        searchBarButtonItem = navigationItem.rightBarButtonItem
        

        self.collectionview.keyboardDismissMode = .onDrag
       
        DispatchQueue.global(qos: .background).async {
            print("Run on background thread")
            
            self.contactList()
        }
       
       self.notificationApi()
        
    }
    func badges(){
        // badge label
        let label = UILabel(frame: CGRect(x: 10, y: -10, width: 22, height: 22))
        label.layer.borderColor = UIColor.clear.cgColor
        label.layer.borderWidth = 2
        label.layer.cornerRadius = label.bounds.size.height / 2
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.font = UIFont(name: "SanFranciscoText-Light", size: 6)
        label.textColor = .white
        label.backgroundColor = .red
        
        label.text = String(self.categoryValue)
        
        // button
        var arr1 = navigationItem.rightBarButtonItems?[1]
        
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 18, height: 16))
        rightButton.setBackgroundImage(UIImage(named: "notification"), for: .normal)
        
        rightButton.addTarget(self, action: #selector(rightButtonTouched), for: .touchUpInside)
        rightButton.addSubview(label)
        // Bar button item
        //
        
        arr1 = UIBarButtonItem(customView: rightButton)
        
        navigationItem.rightBarButtonItems?[1] = arr1 ?? UIBarButtonItem()
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.hideKeyboardWhenTappedAround()
      
    }
    @objc func rightButtonTouched() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:IBACtion------------------
    @IBAction func Menu_btn(Sender:UIButton){
        
        self.toggleLeft()
    }
    @IBAction func searchButtonPressed(sender: AnyObject) {
        showSearchBar()
    }
    @IBAction func btn_notification(sender: AnyObject) {
       
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK:functions,Api functions And Method--------------------------------
    func showSearchBar() {
        searchBar.alpha = 0
        navigationItem.titleView = searchBar
        
        UIView.animate(withDuration: 0.5, animations: {
            self.searchBar.alpha = 1
        }, completion: { finished in
            self.searchBar.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        navigationItem.setLeftBarButton(searchBarButtonItem, animated: true)
        
        UIView.animate(withDuration: 0.3, animations: {
            // self.navigationItem.titleView = self.logoImageView
            // self.logoImageView.alpha = 1
        }, completion: { finished in
            
        })
    }
    
    
    //MARK: UISearchBarDelegate
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        hideSearchBar()
    }
    func categoryListApi(){
       //  KVNProgress.show()
        Network.shared.categoryUser{ (result) in
         //   KVNProgress.dismiss()
            guard let user = result else {
                
                return
            }
            self.categoryListArray = user.categories!
            self.categoryValue = user.unread_count!
            self.filteredCategoryListArray = user.categories!
            self.badges()
            self.collectionview.reloadData()
        }
    }
    
    
}
extension HomeViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if filteredCategoryListArray.count == 0{
            return categoryListArray.count
        }else{
            return filteredCategoryListArray.count
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let homeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        
        homeCell.view_Main.layer.shadowColor = UIColor.lightGray.cgColor
        homeCell.view_Main.layer.shadowOffset = CGSize(width: 5.0, height: 2.0)
        homeCell.view_Main.layer.shadowRadius = 5.0
        homeCell.view_Main.layer.shadowOpacity = 0.3
        homeCell.view_Main.layer.masksToBounds = false
        
        if filteredCategoryListArray.count == 0{
            homeCell.lbl_Title.text = categoryListArray[indexPath.row].name
            
            if let photo = categoryListArray[indexPath.item].image, let url = URL(string: photo) {
                homeCell.img_Logo.af_setImage(withURL: url)
            } else {
                homeCell.img_Logo.image = UIImage(named: "grayscale.png")
            }
            
            
        }
        else{
            homeCell.lbl_Title.text = filteredCategoryListArray[indexPath.row].name
            
            if let photo = filteredCategoryListArray[indexPath.item].image, let url = URL(string: photo) {
                homeCell.img_Logo.af_setImage(withURL: url)
            } else {
                homeCell.img_Logo.image = UIImage(named: "grayscale.png")
            }
            
            
        }
        homeCell.img_Logo.layer.cornerRadius = 40
        homeCell.img_Logo.layer.masksToBounds = true
        return homeCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeSubCategoryViewController") as! HomeSubCategoryViewController
        if filteredCategoryListArray.count == 0{
            
            let categoryid = categoryListArray[indexPath.row].id
            vc.categoryid = categoryid
        }else{
            let categoryid = filteredCategoryListArray[indexPath.row].id
            vc.categoryid = categoryid
        }
        
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: ((self.collectionview.bounds.width - 15)/2), height:180)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
    
    
    
    
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        self.filteredCategoryListArray = self.categoryListArray.filter({(item) -> Bool in
            
            ((item.name?.range(of:searchBar.text!, options: .caseInsensitive)) != nil)
        })
        
        
        self.collectionview.reloadData()
        
        
    }
}

extension HomeViewController{
  
    @objc func contactList(){

        let store = CNContactStore()
        

        store.requestAccess(for: .contacts) { (isGranted, error) in

            // Check the isGranted flag and proceed if true
            
            if isGranted == false {
                DispatchQueue.main.async {
                    self.alertView.isHidden = true
                    self.navigationController?.isNavigationBarHidden = false
                }

         
                return

            }

            let contactStore = CNContactStore()
            print("contactStore")
            let keys = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName),CNContactFamilyNameKey, CNContactPhoneNumbersKey,CNContactEmailAddressesKey] as [Any]

            let request1 = CNContactFetchRequest(keysToFetch: keys  as! [CNKeyDescriptor])

            var cnContacts = [CNContact]()

            try? contactStore.enumerateContacts(with: request1) { (contact, error) in

                cnContacts.append(contact)


            }

          

            for contact in cnContacts {

              

                    for phoneNumber in contact.phoneNumbers {
                     //   let number = (phoneNumber.value ).stringValue
                        if let number = phoneNumber.value as? CNPhoneNumber, let label = phoneNumber.label {
                            let localizedLabel = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel: label)
                            print("\(contact.givenName) \(contact.familyName) tel:\(localizedLabel) -- \(number.stringValue)")
                            var emptyDictionary = [String:String]()
                            emptyDictionary["name"] = (contact.givenName + contact.familyName)
                            emptyDictionary["phone_number"] = number.stringValue
                            self.contactListNamedic.append(emptyDictionary)
                          //  print(self.contactListNamedic)
                            var set = Set<String>()
                            let arraySet: [[String : Any]] = self.contactListNamedic.compactMap {
                                guard let name = $0["phone_number"] else {return nil }
                                return set.insert(name).inserted ? $0 : nil
                            }
                            self.contactListNamedic = arraySet as! [Dictionary<String, String>]
                          //  print(arraySet)

                       // }
                    }


            }

        }
            DispatchQueue.main.async{
                  self.phoneBookApi()
            }
        
          
        }
}
    
    
    //mark:Fetch new contacts-----------------------------------------------
    func phoneBookApi(){
          //KVNProgress.show()
        
                if let loginFirstTym = UserDefaults.standard.value(forKey: "isLoginFristTime") as? String{
                    if loginFirstTym == "true"{
                           alertView.isHidden = false
                        self.navigationController?.isNavigationBarHidden = true
                    }else{
                           alertView.isHidden = true
                        self.navigationController?.isNavigationBarHidden = false
                    }
        
                }
        Network.shared.phoneBook(user_id: UserID ?? 0 , region: "IN", phone_book: contactListNamedic){ (result) in
           //   KVNProgress.dismiss()
              self.alertView.isHidden = true
             UserDefaults.standard.set("false", forKey: "isLoginFristTime")
                self.navigationController?.isNavigationBarHidden = false
            guard let userPhoneBook = result else {
                
                return
            }
            self.contactListPhoneNumberArray = userPhoneBook
            self.newContactAlertShow()
            
            
        }
        
        
    }
    
    func notificationApi(){

        var UserIDPassData:Int!
        if let userID = UserDefaults.standard.value(forKey: "user_id") as? Int{
            UserIDPassData = userID
            
            let userIDPass:String!
            userIDPass =  "\(String(describing: UserIDPassData!))"
            Network.shared.postNotification(name: "", user_id: userIDPass, registration_id: fcmTokenID ?? "", device_id: deviceToken ?? "", type: "ios") { (result) in
                guard result != nil else {
                    
                    return
                }
                
            }
        }
        
      
    }
    
    func newContactAlertShow(){
        
        let tottalnewCount = self.contactListPhoneNumberArray.count
        if tottalnewCount == 0{
            
            print("no contacts")
        }else{
            
            let totalNewContacts =  String(tottalnewCount) + " New contacts in your phone!!!"
            AKAlertController.alert(totalNewContacts, message: "Want to add into any category?", buttons: ["Yes","No"], tapBlock: { (_, touch, index) in
                if index == 1 {return}
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactListViewController") as! ContactListViewController
                vc.homeScreen = "homeScreen"
              
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            )
            
            
        }
    }
}


