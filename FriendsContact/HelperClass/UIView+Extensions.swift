//
//  Extensions.swift
//  BSC
//
//  Created by Sakshi Singh on 02/10/17.
//  Copyright © 2017 Sakshi Singh. All rights reserved.
//

import UIKit

extension UIView {
    
    @IBInspectable var corners: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderColor: UIColor {
        get {
            guard let color = layer.borderColor else {
                return UIColor.clear
            }
            return UIColor(cgColor: color)
        }
        set {
            layer.borderColor = newValue.cgColor
            layer.masksToBounds = true
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    
    @IBInspectable var maxLength: Int {
            get {
                return 10
            }
            set {
                
            }
        }
        
    
    
}

extension UIView {
    class func viewFromNib<T : UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    class func loadNib() -> UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle.main)
    }
    

        func addDashedBorder(strokeColor: UIColor, lineWidth: CGFloat) {
            backgroundColor = .clear
            
            let shapeLayer = CAShapeLayer()
            shapeLayer.name = "DashedTopLine"
            shapeLayer.bounds = bounds
            shapeLayer.position = CGPoint(x: frame.width / 2, y: frame.height / 2)
            shapeLayer.fillColor = UIColor.clear.cgColor
            shapeLayer.strokeColor = strokeColor.cgColor
            shapeLayer.lineWidth = lineWidth
            shapeLayer.lineJoin = kCALineJoinRound
            shapeLayer.lineDashPattern = [4, 4]
            
            let path = CGMutablePath()
            path.move(to: CGPoint.zero)
            path.addLine(to: CGPoint(x: frame.width, y: 0))
            shapeLayer.path = path
            
            layer.addSublayer(shapeLayer)
    }
}

extension UIView {
    
    var x: CGFloat {
        get {
            return self.frame.origin.x
        } set {
            self.frame.origin.x = newValue
        }
    }
    
    var y: CGFloat {
        get {
            return self.frame.origin.y
        } set {
            self.frame.origin.y = newValue
        }
    }
    
    var midX: CGFloat {
        return self.frame.midX
    }
    
    var maxX: CGFloat {
        return self.frame.maxX
    }
    
    var midY: CGFloat {
        return self.frame.midY
    }
    
    var maxY: CGFloat {
        return self.frame.maxY
    }
    
    var width: CGFloat {
        get {
            return self.frame.size.width
        } set {
            self.frame.size.width = newValue
        }
    }
    
    var height: CGFloat {
        get {
            return self.frame.size.height
        } set {
            self.frame.size.height = newValue
        }
    }
    
    var size: CGSize {
        get {
            return self.frame.size
        } set {
            self.frame.size = newValue
        }
    }
    
}

extension UIScreen {
    
    static var width: CGFloat {
        return UIScreen.main.bounds.size.width
    }
    
    static var height: CGFloat {
        return UIScreen.main.bounds.size.height
    }
    
    static var size: CGSize {
        return UIScreen.main.bounds.size
    }
}

extension UILabel {
    
    func manageBlendedLayers() {
        self.isOpaque = true
        self.backgroundColor = UIColor.white
    }
    
}

extension UIColor {
    
    /**
     Creates a UIColor object for the given rgb value which can be specified
     as HTML hex color value. For example:
     
     let color = UIColor(rgb: 0x8046A2)
     let colorWithAlpha = UIColor(rgb: 0x8046A2, alpha: 0.5)
     
     - parameter rgb: color value as Int. To be specified as hex literal like 0xff00ff
     - parameter alpha: alpha optional alpha value (default 1.0)
     */
    
    convenience init(rgb: Int, alpha: CGFloat = 1.0) {
        let r = CGFloat((rgb & 0xff0000) >> 16) / 255
        let g = CGFloat((rgb & 0x00ff00) >>  8) / 255
        let b = CGFloat((rgb & 0x0000ff)      ) / 255
        
        self.init(red: r, green: g, blue: b, alpha: alpha)
    }
    
    convenience init(red: Int, green: Int, blue: Int) {
        let r = CGFloat(red) / 255
        let g = CGFloat(green) / 255
        let b = CGFloat(blue) / 255
        
        self.init(red: r, green: g, blue: b, alpha: 1.0)
    }
    
    convenience public init(hex: Int, alpha: CGFloat = 1.0) {
        let red = CGFloat((hex & 0xFF0000) >> 16) / 255.0
        let green = CGFloat((hex & 0xFF00) >> 8) / 255.0
        let blue = CGFloat((hex & 0xFF)) / 255.0
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    func as1ptImage() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        setFill()
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
        return image
    }
    
    
}

@IBDesignable class RoundedButton: UIButton
{
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateCornerRadius()
    }
    
    @IBInspectable var rounded: Bool = false {
        didSet {
            updateCornerRadius()
        }
    }
    
    func updateCornerRadius() {
        layer.cornerRadius = rounded ? frame.size.height / 2 : 0
    }
}



extension CGPoint {
    static var Center: CGPoint {
        return CGPoint(x: UIScreen.main.bounds.maxX/2, y: UIScreen.main.bounds.maxY/2)
    }
}

extension UIViewController
{
    //NavigationBar Header
    func headernavigation() {
        
    //    navigationController?.setNavigationBarHidden(false, animated: true)
        
        let attributes = [NSAttributedStringKey.font : UIFont(name: "UniversalDoomsday", size: 22)!, NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        let navBackgroundImage:UIImage! = UIImage(named: "header_iPhone")
        let navimg = navBackgroundImage.resizeImageWith(newSize: CGSize(width: UIScreen.main.bounds.size.width, height: navBackgroundImage.size.height))
        self.navigationController?.navigationBar.setBackgroundImage(navimg, for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
  
        self.navigationController?.navigationBar.isTranslucent = false
    }
    

}

extension UIImage{
    
    func resizeImageWith(newSize: CGSize) -> UIImage {
        
        let horizontalRatio = newSize.width / size.width
        let verticalRatio = newSize.height / size.height
        
        let ratio = max(horizontalRatio, verticalRatio)
        let newSize = CGSize(width: size.width * ratio, height: size.height * ratio)
        UIGraphicsBeginImageContextWithOptions(newSize, true, 0)
        draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
extension UIView{

    func tableViewCell() -> UITableViewCell? {
    var tableViewcell : UIView? = self
    while(tableViewcell != nil)
    {
    if tableViewcell! is UITableViewCell {
    break
    }
    tableViewcell = tableViewcell!.superview
    }
    return tableViewcell as? UITableViewCell
    }
    
    
    
    func tableViewIndexPath(_ tableView: UITableView) -> IndexPath? {
    if let cell = self.tableViewCell() {
    return tableView.indexPath(for: cell)
    }
    else {
    return nil
    }
    }
    
    func collectionviewCell() -> UICollectionViewCell? {
    var collectionviewCell : UIView? = self
    while(collectionviewCell != nil)
    {
    if collectionviewCell! is UICollectionViewCell {
    break
    }
    collectionviewCell = collectionviewCell!.superview
    }
    return collectionviewCell as? UICollectionViewCell
    }
    
    func collectionViewIndexPath(_ collectionView: UICollectionView) -> IndexPath? {
    if let cell = self.collectionviewCell() {
    return collectionView.indexPath(for: cell)
    }
    else {
    return nil
    }
    }
    
    
    func txtCornerRadius(_ radius: CGFloat) {
    self.layer.cornerRadius = radius
    self.clipsToBounds = true
    }
    
    
    func buttonShowdow() {
    
    self.layer.shadowOpacity = 1
    self.layer.shadowRadius = 9
    self.layer.shadowColor = UIColor(red: 34/255, green: 156/255, blue: 237/255, alpha: 1.0).cgColor
    self.layer.masksToBounds = false
    }
    
    
    }

extension UIViewController{
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    func convertTimeStampIntoDate(_ timeInMs: Double) -> String
    {
        
        let date = Date(timeIntervalSince1970: timeInMs)
        
        let newDateFormatterForWholeApp = DateFormatter()
        
        newDateFormatterForWholeApp.dateFormat = "HH:mm a"
        let enUSPOSIXLocale = Locale(identifier: "en")
       
        
        newDateFormatterForWholeApp.locale = enUSPOSIXLocale
        
        newDateFormatterForWholeApp.timeZone = TimeZone(identifier: "Europe/London")
        
        let dateStr = newDateFormatterForWholeApp.string(from: date)
        return dateStr
    }
   
  

    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
}
extension UINavigationController {
    
    func backToViewController(vc: Any) {
        // iterate to find the type of vc
        for element in viewControllers as Array {
            if "\(type(of: element)).Type" == "\(type(of: vc))" {
                self.popToViewController(element, animated: true)
                break
            }
        }
    }
    
}


extension Date {
    
    func string(with format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
}
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
