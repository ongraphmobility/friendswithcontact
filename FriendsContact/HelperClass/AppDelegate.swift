//
//  AppDelegate.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 28/11/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import Firebase
import SlideMenuControllerSwift
import CoreTelephony
import UserNotifications
import FirebaseMessaging
import NotificationCenter
import UserNotificationsUI
import Branch


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var leftMenu: MenuViewController!
    var loggedInUserID : String!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.disabledToolbarClasses = [AskQuestionViewController.self]
        IQKeyboardManager.shared.enabledTouchResignedClasses = [AskQuestionViewController.self]
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        DispatchQueue.main.async {
            self.registerForPushNotification()
        }
        if let remoteNotification = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? NSDictionary {
            // there is a notification...do stuff...
            print("dinFInishLaunchingWithOption().. calling didREceiveRemoteNotification")
            
            self.application(application, didReceiveRemoteNotification: remoteNotification as [NSObject : AnyObject])
        }
       // self.sideMeunu()
        
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = UIColor.clear
        }
        
        //MARK:SmsNotification---------------------------------------
        
      //  Branch.setUseTestBranchKey(true)
        // listener for Branch Deep Link data
        Branch.getInstance().initSession(launchOptions: launchOptions) { (params, error) in
            // do stuff with deep link data (nav to page, display content, etc)
            print(params as? [String: AnyObject] ?? {})
            
            let userValue = params as? [String:AnyObject]
            let value_Clicked = userValue?["+clicked_branch_link"] as? Int
            let value_Clicked_UserId = userValue?["user_record_id"] as? Int
            
            let all_UserData = params?["user"] as? [AnyHashable:Any]
            
            if value_Clicked == 1{
                let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                SlideMenuOptions.leftViewWidth = UIScreen.main.bounds.size.width * 2/3
                self.leftMenu = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController
                let homeVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                
                UserDefaults.standard.set(all_UserData, forKey: "AllUSerDataBranch")
                UserDefaults.standard.set(value_Clicked_UserId, forKey: "user_record_id")
                
//                homeVC.addedBy = all_UserData?["added_by"] as? String
//                homeVC.cidName = all_UserData?["cid_name"] as? String
//                homeVC.tagName = all_UserData?["tag"] as? String
//                homeVC.rating =  all_UserData?["rating"] as? String
//                homeVC.serviceNumber = all_UserData?["service_number"] as? String
//                homeVC.cid = all_UserData?["cid"] as? Int
//                homeVC.pid = all_UserData?["pid"] as? Int
//                homeVC.categoryName = all_UserData?["cid_name"] as? String
                
                
                
                let nav = UINavigationController(rootViewController: homeVC)
                nav.isNavigationBarHidden = true
                let sideMenuController = SlideMenuController(mainViewController: nav, leftMenuViewController: self.leftMenu)
                
                self.window?.rootViewController = sideMenuController
                
            }else{
                 self.sideMeunu()
            }
            
            
        }
        
        
        return true
    }
    
    
    
    
    // Respond to URI scheme links
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        // pass the url to the handle deep link call
        let branchHandled = Branch.getInstance().application(application,
                                                             open: url,
                                                             sourceApplication: sourceApplication,
                                                             annotation: annotation
        )
        if (!branchHandled) {
            // If not handled by Branch, do other deep link routing for the Facebook SDK, Pinterest SDK, etc
        }
        
        // do other deep link routing for the Facebook SDK, Pinterest SDK, etc
        return true
    }
    
    // Respond to Universal Links
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        // pass the url to the handle deep link call
        Branch.getInstance().continue(userActivity)
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    //    func applicationDidEnterBackground(_ application: UIApplication) {
    //        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    //        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    //    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        NotificationCenter.default.post(name: NSNotification.Name("NewNumber"), object: nil)
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        connectToFcm()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "FriendsContact")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    func sideMeunu()
    {
        
        if let AlreadyLogin = UserDefaults.standard.value(forKey: "AlreadyLogin") as? String{
            
            if AlreadyLogin == "AlreadyLogin"{
                let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                
                SlideMenuOptions.leftViewWidth = UIScreen.main.bounds.size.width * 2/3
                leftMenu = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController
                let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                
                let nav = UINavigationController(rootViewController: homeVC)
                nav.isNavigationBarHidden = false
                let sideMenuController = SlideMenuController(mainViewController: nav, leftMenuViewController: leftMenu)
                
                self.window?.rootViewController = sideMenuController
                
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                
                SlideMenuOptions.leftViewWidth = UIScreen.main.bounds.size.width * 2/3
                leftMenu = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController
                let homeVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                
                let nav = UINavigationController(rootViewController: homeVC)
                nav.isNavigationBarHidden = true
                let sideMenuController = SlideMenuController(mainViewController: nav, leftMenuViewController: leftMenu)
                
                self.window?.rootViewController = sideMenuController
                
            }
            
        }else{
            
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            
            SlideMenuOptions.leftViewWidth = UIScreen.main.bounds.size.width * 2/3
            leftMenu = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController
            let homeVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            
            let nav = UINavigationController(rootViewController: homeVC)
            nav.isNavigationBarHidden = true
            let sideMenuController = SlideMenuController(mainViewController: nav, leftMenuViewController: leftMenu)
            
            self.window?.rootViewController = sideMenuController
            
        }
        
    }
}
extension AppDelegate {
    
    static var instance: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
}


//MARK:ALL Notification Methods------------------------------------------------------
extension AppDelegate: UNUserNotificationCenterDelegate, MessagingDelegate{
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        Messaging.messaging().disconnect()
        print("Disconnected from FCM.")
    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // some other way of handling notification
        completionHandler([.alert, .sound])
        // self.fireBaseFetchData()
        self.dataFtechData()
    }
    
    //    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    //
    //          deviceTokenString = deviceToken.hexString()
    //        UserDefaults.standard.set(deviceTokenString, forKey: "deviceTokenString")
    //    }
    //
    
    func registerForPushNotification()
    {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
                // Enable or disable features based on authorization.
                if granted == true{
                    debugPrint("Allow")
                    OperationQueue.main.addOperation {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
                else{
                    debugPrint("Don't Allow")
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    func application(_ application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = extractTokenFromData(deviceToken: deviceToken)
        
        print("sign vala - \(deviceTokenString)")
        Messaging.messaging().apnsToken = deviceToken
        UserDefaults.standard.set(deviceTokenString, forKey: "deviceToken")
    }
    
    func extractTokenFromData(deviceToken:Data) -> String {
        let token = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        return token.uppercased();
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        
        print("Firebase registration token: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: "fcmtoken")
        connectToFcm()
    }
    func connectToFcm() {
        
        
        Messaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
                
            }
        }
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("dddd", userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
        // fireBaseFetchData()
        Branch.getInstance().handlePushNotification(userInfo)
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        //  return Twitter.sharedInstance().application(app, open: url, options: options)
        
        print( url.scheme!)
        
        Branch.getInstance().application(app, open: url, options: options)
        return true
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        debugPrint("Tapped in notification")
        let userInfo = response.notification.request.content.userInfo
        handleNotification(userInfo, application: UIApplication.shared)
        
        
        //
        //        print(response.notification.request.content.userInfo)
        //        debugPrint("Tapped show",response.actionIdentifier)
        //
        //        print(response.notification.request.content.userInfo["gcm.notification.EnrollmentId"])
        //        //gcm.notification.StudentEnrollNo
        //        UIApplication.shared.applicationIconBadgeNumber = 0
        //
        //        let center = UNUserNotificationCenter.current()
        //        center.removeAllDeliveredNotifications() // To remove all delivered notifications
        //        center.removeAllPendingNotificationRequests()
    }
    
    func dataFtechData(){
        if(loggedInUserID == nil){
            loggedInUserID = Auth.auth().currentUser?.uid
        }
        
    }
    
    //MARK: HANDLE NOTIFICATION
    func handleNotification(_ userInfo: [AnyHashable: Any],application:UIApplication)
    {
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        if UIApplication.shared.applicationIconBadgeNumber > 0 {
            
            UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber - 1
        }
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        SlideMenuOptions.leftViewWidth = UIScreen.main.bounds.size.width * 2/3
        leftMenu = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController
        let homeVC = storyboard.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        let nav = UINavigationController(rootViewController: homeVC)
        // nav.isNavigationBarHidden = true
        let sideMenuController = SlideMenuController(mainViewController: nav, leftMenuViewController: leftMenu)
        self.window?.rootViewController = sideMenuController
        //        let valueAlert = userInfo["alert"] as? String
        //        let dict = convertToDictionary(text: valueAlert!)
        //        print(dict)
        //        switch dict?["screen_tag"] as? String   {
        //
        //        case "number_verify":
        //             let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        //            SlideMenuOptions.leftViewWidth = UIScreen.main.bounds.size.width * 2/3
        //            leftMenu = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as? MenuViewController
        //            let homeVC = storyboard.instantiateViewController(withIdentifier: "SmsNotificationViewController") as! SmsNotificationViewController
        //             homeVC.addedBy = dict?["added_by"] as? String
        //             homeVC.cidName = dict?["cid_name"] as? String
        //             homeVC.tagName = dict?["tag"] as? String
        //             homeVC.rating =  dict?["rating"] as? String
        //             homeVC.serviceNumber = dict?["service_number"] as? String
        //             homeVC.cid = dict?["cid"] as? Int
        //             homeVC.pid = dict?["pid"] as? Int
        //             homeVC.categoryName = dict?["cid_name"] as? String
        
        //
        //
        //            let nav = UINavigationController(rootViewController: homeVC)
        //            nav.isNavigationBarHidden = true
        //            let sideMenuController = SlideMenuController(mainViewController: nav, leftMenuViewController: leftMenu)
        //
        //            self.window?.rootViewController = sideMenuController
        //        case "remittanceApprove": //Open Home only
        //            break
        
        //
        //        default:
        //            break
    }
}

func convertToDictionary(text: String) -> [String: Any]? {
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
        }
    }
    return nil
}


