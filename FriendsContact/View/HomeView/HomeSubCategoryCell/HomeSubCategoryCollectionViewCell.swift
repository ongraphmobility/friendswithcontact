//
//  HomeSubCategoryCollectionViewCell.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 29/11/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit

class HomeSubCategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_Title:UILabel!
    @IBOutlet weak var lbl_First:UILabel!
    @IBOutlet weak var lbl_PointFirst:UILabel!
    @IBOutlet weak var img_SubCategory:UIImageView!
    override var isSelected: Bool {
        didSet {
            if isSelected { // Selected cell
                self.backgroundColor = UIColor.init(hex:  0x418DE6)
                self.lbl_Title?.textColor = UIColor.white
            } else { // Normal cell
                self.backgroundColor = UIColor.white
                self.lbl_Title?.textColor = UIColor.darkGray
            }
        }
    }

}
