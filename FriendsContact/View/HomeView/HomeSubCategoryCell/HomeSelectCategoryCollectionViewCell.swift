//
//  HomeSubCategoryCollectionViewCell.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 29/11/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit

class HomeSelectCategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var view_Tick:UIImageView!
    @IBOutlet weak var img_Logo:UIImageView!
    @IBOutlet weak var lbl_Title:UILabel!
    @IBOutlet weak var view_Main:UIView!
}
