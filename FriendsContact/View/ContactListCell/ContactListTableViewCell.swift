//
//  ContactListTableViewCell.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 30/11/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit

class ContactListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var img_Logo:UIImageView!
    @IBOutlet weak var lbl_Title:UILabel!
      @IBOutlet weak var lbl_Short:UILabel!
     @IBOutlet weak var lbl_Number:UILabel!
     @IBOutlet weak var lbl_addedBy:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
