//
//  AboutTableViewCell.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 28/12/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit

class AboutTableViewCell: UITableViewCell {
    @IBOutlet weak var lbl_topic:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
