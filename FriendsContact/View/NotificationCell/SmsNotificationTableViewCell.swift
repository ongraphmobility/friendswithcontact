//
//  SmsNotificationTableViewCell.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 04/03/19.
//  Copyright © 2019 Sakshi Singh. All rights reserved.
//

import UIKit
import FloatRatingView

class SmsNotificationTableViewCell: UITableViewCell,FloatRatingViewDelegate,UITextViewDelegate {
    
    @IBOutlet weak var btn_Mark:UIButton!
    @IBOutlet weak var lbl_Main:UILabel!
    @IBOutlet weak var lbl_Name:UILabel!
    @IBOutlet weak var lbl_Number:UILabel!
    @IBOutlet weak var view_Rating: FloatRatingView!
    @IBOutlet weak var lbl_Rating: UILabel!
    @IBOutlet weak var lbl_Last:UITextView!

    
    var liveLabel: String!
    var updatedLabel: String!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      //  view_Rating.rating = Double(3.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: FloatRatingViewDelegate
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        
        liveLabel = String(format: "%.1f", view_Rating.rating)
        
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        
        updatedLabel = String(format: "%.1f", view_Rating.rating)
    }
    //MARK:Rating function called on viewdidLoad--------------------------------------
    func ratingfunction(){
        view_Rating.type = .wholeRatings
        // Reset float rating view's background color
        view_Rating.backgroundColor = UIColor.clear
        
        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        view_Rating.delegate = self
        view_Rating.contentMode = UIViewContentMode.scaleAspectFit
        liveLabel = String(format: "%.1f", view_Rating.rating)
        updatedLabel = String(format: "%.1f", view_Rating.rating)
        
    }

}
