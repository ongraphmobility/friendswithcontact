//
//  NotificationTableViewCell.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 11/03/19.
//  Copyright © 2019 Sakshi Singh. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbl_Title:UILabel!
    @IBOutlet weak var lbl_Description:UILabel!
    @IBOutlet weak var lbl_Time:UILabel!
    @IBOutlet weak var img_View:UIImageView!
    @IBOutlet weak var view_Main:UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
