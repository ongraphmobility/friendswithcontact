//
//  MenuCellTableViewCell.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 29/11/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit

class MenuCellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbl_MenuTitle:UILabel!
    @IBOutlet weak var img_Menu:UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
