//
//  AddContactsTableViewCell.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 03/12/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit

class AddContactsTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_NAme:UILabel!
    @IBOutlet weak var lbl_Number:UILabel!
    @IBOutlet weak var lbl_Short:UILabel!
    @IBOutlet weak var lbl_Rating:UILabel!
    @IBOutlet weak var lbl_Show:UILabel!
     @IBOutlet weak var lbl_Verified:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
