//
//  AskQuestionTableViewCell.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 30/11/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit

class AskQuestionTableViewCell: UITableViewCell,UITextViewDelegate {
    
   
    
    
    @IBOutlet weak var lbl_USerName:UILabel!
    @IBOutlet weak var lbl_Comment:UILabel!
    @IBOutlet weak var lbl_Short:UILabel!
    @IBOutlet weak var btn_Send:UIButton!
    @IBOutlet weak var btn_Answer:UIButton!
    @IBOutlet weak var txt_reply:UITextView!
    @IBOutlet weak var textViewHeightConstraint:NSLayoutConstraint!
    @IBOutlet weak var ViewHeightConstraint:NSLayoutConstraint!
    
      //let textViewMaxHeight: CGFloat = 70
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txt_reply.delegate = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
  
}
