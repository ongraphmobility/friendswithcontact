//
//  MyQuestionTableViewCell.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 14/12/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit

class MyQuestionTableViewCell: UITableViewCell,UITextViewDelegate {

    @IBOutlet weak var lbl_USerName:UILabel!
    @IBOutlet weak var lbl_Comment:UILabel!
    @IBOutlet weak var lbl_Short:UILabel!
    @IBOutlet weak var btn_Send:UIButton!
    @IBOutlet weak var txt_reply:UITextView!
    @IBOutlet weak var btn_Answer:UIButton!
   // let textViewMaxHeight: CGFloat = 50
    @IBOutlet weak var ViewHeightConstraint:NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txt_reply.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    func textViewDidChange(_ textView: UITextView) {
//      print("noteText text")
//        if txt_reply.contentSize.height >= self.textViewMaxHeight
//{
//            txt_reply.isScrollEnabled = true
//        }
//        else
//        {
//            txt_reply.frame.size.height = txt_reply.contentSize.height
//            txt_reply.isScrollEnabled = false
//        }
//
//    }

    
}
