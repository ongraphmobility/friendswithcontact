//
//  AskAnswerTableViewCell.swift
//  FriendsContact
//
//  Created by Sakshi Singh on 14/12/18.
//  Copyright © 2018 Sakshi Singh. All rights reserved.
//

import UIKit

class AskAnswerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbl_userName:UILabel!
    @IBOutlet weak var lbl_msg:UILabel!
    @IBOutlet weak var lbl_shortName:UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
