//
//  Network.swift
//  Tend
//
//  Created by Sakshi Singh on 14/04/18.
//  Copyright © 2018 Sakshi. All rights reserved.
//

import Foundation
import Alamofire
import KVNProgress


typealias ServiceResponse = (Any?, Error?) -> Void

class Network: NSObject {
    
    
    
    //Live Url
    static let shared: Network = Network(baseURL:URL.init(string: "http://demo2.ongraph.com:3000/api/v1/")!)
    let baseURL: URL
    var headers: HTTPHeaders
    let decoder = JSONDecoder()
    
    
    // Initialization
    private init(baseURL: URL) {
        self.baseURL = baseURL
        
        self.headers = [
            //  "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded",
            
        ]
    }
    

    func loginUser(phone_number: String,region:String, completion:@escaping (_ result:LoginModel?) -> Void) {
        let para: Parameters =
            ["phone_number":phone_number,"region":region]
        // print(para)
        let url = "http://18.223.127.134/api/v1/accounts/login/"
       //   let url = "http://demo2.ongraph.com:3000/api/v1/accounts/login/"
        Alamofire.request(url, method: .post, parameters:para , encoding: URLEncoding.default, headers: nil).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                if data.success == "true"{
                    completion(data)
                }
                
            }
            catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    func logout(completion:@escaping (_ result:LoginModel?) -> Void) {
       
        //let url = "http://18.223.127.134/api/v1/accounts/login/"
      
        var id = String()
        if let phoneNumberId = UserDefaults.standard.value(forKey: "phoneNumber") as? Int{
            
            id = String(phoneNumberId)
        }
        //  let url = "http://demo2.ongraph.com:3000/api/v1/accounts/logout/?uph=" + id
        
        let url = "http://18.223.127.134/api/v1/accounts/logout/?uph=" + id
        
        
        Alamofire.request(url, method: .post, parameters:nil , encoding: URLEncoding.default, headers: nil).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                if data.success == "true"{
                    completion(data)
                }
                
            }
            catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    func markVerify(completion:@escaping (_ result:LoginModel?) -> Void) {
        
        //let url = "http://18.223.127.134/api/v1/accounts/login/"
        
        var id = String()
        if let phoneNumberId = UserDefaults.standard.value(forKey: "UserIDNotification") as? Int{
            
            id = String(phoneNumberId)
        }
      //  let url = "http://demo2.ongraph.com:3000/api/v1/contacts_management/service-number/verify/?user_record_id=" + id
          let url = "http://18.223.127.134/api/v1/contacts_management/service-number/verify/?user_record_id=" + id
        Alamofire.request(url, method: .post, parameters:nil , encoding: URLEncoding.default, headers: nil).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                if data.success == "true"{
                    completion(data)
                }
                
            }
            catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    func addContact(parent_category: Int,child_category: Int,user_phone_number: String,phone_number: String,rating: String,name_tags: String,id:Int,service_number:String ,completion:@escaping (_ result:LoginModel?) -> Void) {
        let para: Parameters =
            ["parent_category":parent_category,"child_category":child_category,"user_phone_number":user_phone_number,"phone_number":phone_number,"rating":rating,"name_tags":name_tags,"id":id,"service_number":service_number]
         print(para)
        let url = "http://18.223.127.134/api/v1/contacts_management/service-number/"
       // let url = "http://demo2.ongraph.com:3000/api/v1/contacts_management/service-number/"
       
        Alamofire.request(url, method: .post, parameters:para , encoding:
        JSONEncoding.default, headers: nil).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(LoginModel.self, from: resposne.data ?? Data())
                print("Success")
                
                if data.success == "true"{
                    completion(data)
                    
                }
                // completion(data)
                // AKAlertController.alert("Alert", message: "Contact Added")
            }
            catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func postQuestion(posted_by_id: Int,question: String,completion:@escaping (_ result:AllQuestion?) -> Void) {
        let para: Parameters =
            ["posted_by_id":posted_by_id,"question":question]
        // print(para)
       let url = "http://18.223.127.134/api/v1/ask_questions/questions/"
       // let url = "http://demo2.ongraph.com:3000/api/v1/ask_questions/questions/"
        Alamofire.request(url, method: .post, parameters:para  , encoding: JSONEncoding.default, headers: nil).response { (resposne) in
            KVNProgress.show()
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(PostQuestion.self, from: resposne.data ?? Data())
                print("Success")
                  KVNProgress.dismiss()
                AKAlertController.alert("Thank you for submitting your question.")
                completion(data.data)
               
            }
            catch let error {
                  KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    
    
    func phoneBook(user_id: Int,region: String,phone_book:[[String:Any]],completion:@escaping (_ result:[NewNumber]?) -> Void) {
        let para: Parameters =
            ["user_id":user_id,"region":region,"phone_book":phone_book]
        // print(para)
       let url = "http://18.223.127.134/api/v1/accounts/phone-book/"
      // let url = "http://demo2.ongraph.com:3000/api/v1/accounts/phone-book/"
        Alamofire.request(url, method: .post, parameters:para  , encoding: JSONEncoding.default, headers: nil).response { (resposne) in
         //   KVNProgress.show()
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(PhoneBook.self, from: resposne.data ?? Data())
                print("Success")
             //   KVNProgress.dismiss()
           //     AKAlertController.alert("User phone book has been successfully uploaded")
                completion(data.new_numbers)
                
            }
            catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    
    
    func createAnswer(question_id: Int,answer: String,answered_by_id: Int,completion:@escaping (_ result:CreateAnswer) -> Void) {
        let para: Parameters =
            ["question_id":question_id,"answer":answer,"answered_by_id":answered_by_id]
        // print(para)
       let url = "http://18.223.127.134/api/v1/ask_questions/answers/"
       // let url = "http://demo2.ongraph.com:3000/api/v1/ask_questions/answers/"
        Alamofire.request(url, method: .post, parameters:para  , encoding: JSONEncoding.default, headers: nil).response { (resposne) in
              KVNProgress.show()
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(CreateAnswer.self, from: resposne.data ?? Data())
                print("Success")
                  KVNProgress.dismiss()
               
                completion(data)
                
            }
            catch let error {
                   KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    func createAnswerList(question_id: Int,answer: String,answered_by_id: Int,completion:@escaping (_ result:Answer?) -> Void) {
        let para: Parameters =
            ["question_id":question_id,"answer":answer,"answered_by_id":answered_by_id]
        // print(para)
    let url = "http://18.223.127.134/api/v1/ask_questions/answers/"
    //    let url = "http://demo2.ongraph.com:3000/api/v1/ask_questions/answers/"
        Alamofire.request(url, method: .post, parameters:para  , encoding: JSONEncoding.default, headers: nil).response { (resposne) in
               KVNProgress.show()
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(CreateAnswer.self, from: resposne.data ?? Data())
                print("Success")
                   KVNProgress.dismiss()
                AKAlertController.alert("Thank you for submitting your answer.")
                completion(data.data)
                
            }
            catch let error {
                   KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    func postNotification(name: String,user_id: String,registration_id: String,device_id: String,type: String,completion:@escaping (_ result:Answer?) -> Void) {
        let para: Parameters =
            ["name":name,"user_id":user_id,"registration_id":registration_id,"device_id":device_id,"type":type]
        // print(para)
          let url = "http://18.223.127.134/api/v1/devices/"
       // let url = "http://demo2.ongraph.com:3000/api/v1/devices/"
        Alamofire.request(url, method: .post, parameters:para  , encoding: JSONEncoding.default, headers: nil).response { (resposne) in
            KVNProgress.show()
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(CreateAnswer.self, from: resposne.data ?? Data())
                print("Success")
                KVNProgress.dismiss()
               
                completion(data.data)
                
            }
            catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    //MARK:Get Methods---------------------------------------------------------
    
    func categoryUser(completion:@escaping (_ result:HomeCategory?) -> Void) {
        var id = String()
        if let phoneNumberId = UserDefaults.standard.value(forKey: "phoneNumber") as? Int{
            
          id = String(phoneNumberId)
            }
       
        let url = "http://18.223.127.134/api/v1/contacts_management/categories/?uph=" + id
      // let url = "http://demo2.ongraph.com:3000/api/v1/contacts_management/categories/?uph=" + id
        Alamofire.request(url, method: .get, parameters:nil  , encoding: URLEncoding.default, headers: nil).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(HomeCategory.self, from: resposne.data ?? Data())
                print("Success")
                 completion(data)

            }
            catch let error {
                 KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    func notificationList(completion:@escaping (_ result:[NotificationList]?) -> Void) {
        var id = String()
      
        var url = String()
        if let phoneNumberId = UserDefaults.standard.value(forKey: "phoneNumber") as? Int{
            
            id = String(phoneNumberId)
            url = "http://18.223.127.134/api/v1/notifications/?uph=" + id
          //  url = "http://demo2.ongraph.com:3000/api/v1/notifications/?uph=" + id
        }

        
        Alamofire.request(url, method: .get, parameters:nil  , encoding: URLEncoding.default, headers: nil).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(NotificationModel.self, from: resposne.data ?? Data())
                print("Success")
                completion(data.data)
                
            }
            catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func notificationListRead(completion:@escaping (_ result:[NotificationList]?) -> Void) {
       
        var notiID = String()
        var url = String()
        var id = String()
     
        if let phoneNumberId = UserDefaults.standard.value(forKey: "phoneNumber") as? Int{
            
            id = String(phoneNumberId)
            
        }
        if let notificationId = UserDefaults.standard.value(forKey: "NotificationTapId") as? Int{
        
                    notiID = String(notificationId)
            
                }
        // url = "http://demo2.ongraph.com:3000/api/v1/notifications/?uph=" + id + "&id=" + notiID
        url = "http://18.223.127.134/api/v1/notifications/?uph=" + id + "&id=" + notiID
        
        
        Alamofire.request(url, method: .get, parameters:nil  , encoding: URLEncoding.default, headers: nil).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(NotificationModel.self, from: resposne.data ?? Data())
                print("Success")
                completion(data.data)
                
            }
            catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func getContact(completion:@escaping (_ result:[ContactAdded]?) -> Void) {
        
        // print(para)
         var id = String()
        if let parentID = UserDefaults.standard.value(forKey: "parentid") as? Int{
            let childID = UserDefaults.standard.value(forKey: "childId") as? Int
            let phoneNumberId = UserDefaults.standard.value(forKey: "phoneNumber") as? Int
            
            id = (String(parentID) + "&cid=" + String(childID ?? 0) + "&uph=" + String(phoneNumberId ?? 0))
            
        }
        
    let url = ("http://18.223.127.134/api/v1/contacts_management/user-record/?pid=" + id)
   // let url = ("http://demo2.ongraph.com:3000/api/v1/contacts_management/user-record/?pid=" + id)
        Alamofire.request(url, method: .get, parameters:nil  , encoding: URLEncoding.default, headers: nil).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(GetAddedContact.self, from: resposne.data ?? Data())
                print("Success")
                completion(data.data)
                
            }
            catch let error {
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    
    
    func getAllQuestion(completion:@escaping (_ result:GetAllQuestion?) -> Void) {
        
        // print(para)
        var id = String()
        if let phoneNumberId = UserDefaults.standard.value(forKey: "phoneNumber") as? Int{
            
            
            id = String(phoneNumberId)
            
        }
       let url = "http://18.223.127.134/api/v1/ask_questions/questions/?uph=" + id
      //  let url = "http://demo2.ongraph.com:3000/api/v1/ask_questions/questions/?uph=" + id
        Alamofire.request(url, method: .get, parameters:nil  , encoding: URLEncoding.default, headers: nil).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(GetAllQuestion.self, from: resposne.data ?? Data())
                print("Success")
                completion(data)
                
            }
            catch let error {
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    func getMyAllQuestion(completion:@escaping (_ result:GetAllQuestion?) -> Void) {
        
          var id = String()
        if let phoneNumberId = UserDefaults.standard.value(forKey: "phoneNumber") as? Int{
          
            
            id = String(phoneNumberId)
            
        }
let url = "http://18.223.127.134/api/v1/ask_questions/questions/?uph=" + id
//let url = "http://demo2.ongraph.com:3000/api/v1/ask_questions/questions/?uph=" + id
        Alamofire.request(url, method: .get, parameters:nil  , encoding: URLEncoding.default, headers: nil).response { (resposne) in
            
            self.printRequest(resposne)
             
            do {
                let data = try self.decoder.decode(GetAllQuestion.self, from: resposne.data ?? Data())
                print("Success")
                completion(data)
                
            }
            catch let error {
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
    
    func getAllAnswer(completion:@escaping (_ result:GetAllAnswer?) -> Void) {
        
        // print(para)
        
        var id = String()
        if let questionId = UserDefaults.standard.value(forKey: "Question_id") as? Int{
           id = String(questionId)
            
        }
      
        if let phoneNumberId = UserDefaults.standard.value(forKey: "phoneNumber") as? Int{
            
            
            id = id + "&uph=" + String(phoneNumberId)
            
        }
        let url = "http://18.223.127.134/api/v1/ask_questions/answers/?qid=" + id
       // let url = "http://demo2.ongraph.com:3000/api/v1/ask_questions/answers/?qid=" + id
        Alamofire.request(url, method: .get, parameters:nil  , encoding: URLEncoding.default, headers: nil).response { (resposne) in
            
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(GetAllAnswer.self, from: resposne.data ?? Data())
                print("Success")
                completion(data)
                
            }
            catch let error {
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
    
  
    
    
    func deleteContact(completion:@escaping (_ result:ContactAdded?) -> Void) {
        var id = String()
        var phoneNumber = String()
        var serviceNum = String()
        if let phoneNumberId = UserDefaults.standard.value(forKey: "phoneNumber") as? Int{
            phoneNumber = String(phoneNumberId)
            
            id = id + "?uph=" + String(phoneNumberId)
            
        }
        if let service_number = UserDefaults.standard.value(forKey: "service_number") as? String{
            serviceNum = service_number
//            if service_number.first == "+"{
//
//            let jhdgfhjdgs = service_number.replacingCharacters(in: ...service_number.startIndex, with: "")
//
//                print(jhdgfhjdgs)
//                let djhjkd = "%2B" + String(jhdgfhjdgs)
//                id = id + "&sph=" +  djhjkd
//            }
//            else{
//                 id = id + "&sph=" +  service_number
//
//            }
              id = id + "&sph=" +  service_number
            
       }
     let url = "http://18.223.127.134/api/v1/contacts_management/service-number/delete/"
        
        let safeURL = url.addingPercentEncoding(withAllowedCharacters:  .urlQueryAllowed)
        
        let parameters: Parameters = [
            "uph": phoneNumber,
            "sph": serviceNum
        ]
        
    //let url = "http://demo2.ongraph.com:3000/api/v1/ask_questions/questions/" + id + "/"
        Alamofire.request(safeURL!, method: .post, parameters:parameters  , encoding: URLEncoding(destination: .queryString), headers: nil).response { (resposne) in
            KVNProgress.show()
            self.printRequest(resposne)
            
            do {
                let data = try self.decoder.decode(ContactAdded.self, from: resposne.data ?? Data())
                print("Success")
                KVNProgress.dismiss()
                AKAlertController.alert("Contact is successfully deleted from category.")
                
                completion(data)
                
            }
            catch let error {
                KVNProgress.dismiss()
                self.handleError(data: resposne, error: error)
            }
        }
        
    }
   
}




extension Network {
    // to hadle error of api on response
    private func handleError(data: DefaultDataResponse?, error:Error?){
        if data?.response == nil {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
              //  KVNProgress.dismiss()
                AKAlertController.actionSheet("", message: "Internet connection failed.", sourceView: UIApplication.shared.keyWindow?.rootViewController?.presentedViewController?.view as Any, buttons: ["Try Again"], color: nil, tapBlock: nil)
            }
            return
        }
        guard let errorV = try? self.decoder.decode(ErrorModel.self, from: data?.data ?? Data()) else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                print(error!)
              //  KVNProgress.dismiss()
                AKAlertController.alert("Alert", message: error?.localizedDescription ?? "")
            }
            return
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            guard errorV.code != 200 else { return }
            //  KVNProgress.dismiss()
            AKAlertController.alert("Alert", message: errorV.message)
        }
    }
    
    // To print request
    private func printRequest(_ response: DefaultDataResponse) {
        
        _ = (try? JSONSerialization.jsonObject(with: response.request?.httpBody ?? Data(), options: []))
        
        //  let decryptParam = try? Network.decryptApiResponse(responseData: (decryptParams?["data"] as? String ?? "").data(using: .utf8) ?? Data())
        
        printDebug(
            """
            \n
            API Request:-
            --------------
            URL         : \(response.request?.url?.absoluteString ?? "")
            Headers     : \(response.request?.allHTTPHeaderFields ?? [:])
            HTTP Method : \(response.request?.httpMethod ?? "")
            Parameters  : \(String(bytes: response.request?.httpBody ?? Data(), encoding: .utf8) ?? "")
            RequestStatusCode: \(response.response?.statusCode ?? 0)
            
            Response    : \(String(bytes: response.data ?? Data(), encoding: .utf8) ?? "")
            \n\n
            """
        )
        
        do {
            let decodedJson = try JSONSerialization.jsonObject(with: response.data ?? Data(), options: [])
            if let json = decodedJson as? Dictionary<String, Any>, (json["code"] as? Int) == 401 {
                //                AppUserDefaults.removeAllValues()
                //               AppDelegate.instance.setRootViewController()
            }
        } catch _ {
        }
    }
    
}

func printDebug<T>(_ obj: T) {
    //MARK:- Comment Line to disable all logs
    print(obj)
}

func jsonString(from object:Any) -> String? {
    guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
        return nil
    }
    return String(data: data, encoding: String.Encoding.utf8)
}




class MyError: NSError {
    var customDescription: String = ""
    init() {
        super.init(domain: "MyError", code: 100, userInfo: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
